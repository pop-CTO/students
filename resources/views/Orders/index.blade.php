@extends ('layouts.master')
@section('title', "الفواتير ")
@section ('content')

<div class="row">

<div class="col-md-12 col-sm-12">

</div>

<div class="col-md-12">


    <!-- begin panel -->
    <div class="panel panel-inverse">
        <div class="panel-heading">
            <div class="panel-heading-btn">
              <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
              <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
              <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>


            </div>
            <h4 class="panel-title">الفواتير </h4>
        </div>
        <div class="panel-body">
                         <!--POP Add this to show  sendMessageFalid -->
                         @if(Session::get("sendMessageSucc"))
                         <div class="alert alert-success fade in m-b-15 text-center">
                                                   <strong>  {{Session::get("sendMessageSucc")}}!</strong>
                   
                                                   <span class="close" data-dismiss="alert">×</span>
                                               </div>
                         @endif
                         <!--POP End  -->

            <table id="data-table"
             class="table table-striped table-bordered nowrap" width="100%" dir="rtl" >
                <thead>
                    <tr>
                      <th>رقم الفاتورة </th>
                      <th>اسم المستخدم </th>
                      <th>العنوان </th>
                      <th>السعر الكلي </th>
                      <th>حالة الفاتورة  </th>

                      <th>الموافقة علي الطلب  </th>
                      <th>الغاء الطلب  </th>
                      <th>تاريخ التسجيل </th>
                      
                    </tr>
                </thead>
                <tbody>
                    @foreach ($billes as $bille)
                    <tr>
                      <td><a href="/orders/show/{{ $bille->id }}">{{ $bille->id }}</a></td>
                      <td><a href="/users/show/{{ $bille->user_id }}">{{$bille->users->name }}</a> </td>
                      <td>{{ $bille->address }}</td>
                      <td>{{ $bille->total_price }}</td>
                      @foreach ($bille->orders  as  $status)
                          
                      @endforeach
                      @if($status->status == 'request')
                      <td>جاري التنفيذ</td>
                      <td><a class="btn btn-info" href="/orders/done/{{ $bille->id }}">موافقة </a></td>
                      <td><a class="btn btn-danger"  href="/orders/finsih/{{ $bille->id }}">رفض</a></td>
                  
                      @elseif($status->status == 'done')
                      <td>تم الموافقة</td>
                      <td>--------</td>
                      <td><a class="btn btn-danger"  href="/orders/finsih/{{ $bille->id }}">رفض</a></td>
                     
                      @else
                      <td>تم الرفض </td>
                      <td><a class="btn btn-info" href="/orders/done/{{ $bille->id }}">موافقة </a></td>
                      <td>--------</td>
                      @endif
                      <td>{{ $bille->created_at }}</td>



                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <!-- end panel -->
</div>

</div><!-- end row -->
@endsection




