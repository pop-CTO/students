@extends ('layouts.master')
@section('title', "الفاتورة")
@section ('content')

<div class="row">

<div class="col-md-12 col-sm-12">

</div>

<div class="col-md-12">


    <!-- begin panel -->
    <div class="panel panel-inverse">
        <div class="panel-heading">

            <h4 class="panel-title">الفاتورة</h4>
        </div>
        <div class="panel-body">
                <!-- begin profile-section -->
                <div class="profile-section">

                    <!-- begin profile-right -->
                    <div class="profile">
                        <!-- begin profile-info -->
                        <div class="profile-info">
                            <!-- begin table -->
                            <div class="table-responsive">
                                <table class="table table-profile">
                                    <thead>
                                    </thead>
                                    <tbody>
                                        <tr class="highlight">
                                            <td class="field">رقم الفاتورة </td>
                                            <td><a href="#">{{$billes->id}}</a></td>
                                        </tr>
                                        <tr class="highlight">
                                            <td class="field">اسم المستخدم  </td>
                                            @if($bille->isVisitor == 0)
                                            <td><a href="/users/show/{{ $billes->user_id }}">{{$billes->users->name }}</a></td>
                                            @else
                                            <td>{{$billes->users->name }}></td>
                                            @endif
                                        </tr>

                                        

                                        <tr class="highlight">
                                            <td class="field"> العنوان </td>
                                            <td>{{ $billes->address }}</td>
                                        </tr>


                                        <tr class="highlight">
                                            <td class="field">المبلغ الكلي </td>
                                            <td>{{ $billes->total_price }}</td>
                                        </tr>


                                        <tr class="highlight">
                                            <td class="field"> حالة الفاتورة </td>
                                            <td>{{ $billes->orders[0]->status }}</td>
                                        </tr>


                                        <tr class="highlight">
                                            <td class="field"> تاريخ التسجيل </td>
                                            <td>{{ $billes->created_at }}</td>
                                        </tr>


                                        <tr class="highlight">
                                            <td class="field">  رقم الهاتف</td>
                                            <td>{{ $billes->phone }}</td>
                                        </tr>
                                        <tr class="highlight">
                                            <td class="field">المنطقة </td>
                                            <td>{{ $billes->district }}</td>
                                        </tr>
                                        <tr class="highlight">
                                            <td class="field">الشارع  </td>
                                            <td>{{ $billes->street }}</td>
                                        </tr>

                                        <tr class="highlight">
                                            <td class="field">رقم المنزل </td>
                                            <td>{{ $billes->home }}</td>
                                        </tr>

                                        <tr class="highlight">
                                            <td class="field">طريقة الدفع   </td>
                                            <td>{{ $billes->payment_type }}</td>
                                        </tr>

                                    </tr>


                                </tr>


                                    </tbody>
                                </table>
                            </div>
                            <!-- end table -->


                        </div>
                        <!-- end profile-info -->
                    </div>
                    <!-- end profile-right -->
                </div>
                <!-- end profile-section -->
        </div>
    </div>


    
    <!-- end panel -->
</div>




<div class="col-md-12">


    <!-- begin panel -->
    <div class="panel panel-inverse">
        <div class="panel-heading">
            <div class="panel-heading-btn">
              <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
              <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
              <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>


            </div>
            <h4 class="panel-title">الطالبات  </h4>
        </div>
        <div class="panel-body">
         

            <table id="data-table"
             class="table table-striped table-bordered nowrap" width="100%" dir="rtl" >
                <thead>
                    <tr>
                      <th>رقم الطلب  </th>
                      <th>المنتج </th>
                      <th>الكمية  </th>
                      <th>حالة الطلب   </th>
                      <th>تاريخ التسجيل </th>
                      
                    </tr>
                </thead>
                <tbody>
                    @foreach ($billes->orders as $bille)
                 
                   
                    <tr>
                        <td>{{ $bille->id }}</td>
                        <td><a href="/product/show/{{ $bille->prodect_id }}">{{$bille->order_product->name }}</a> </td>
                        <td>{{ $bille->mount }}</td>
                        <td>{{ $bille->status }}</td>
                        <td>{{ $bille->created_at }}</td>

                    </tr>
                    @endforeach
                </tbody>
            </table>

        </div>
    
    </div>
    <!-- end panel -->
</div>
    
    <!-- end panel -->
</div>
</div><!-- end row -->
@endsection