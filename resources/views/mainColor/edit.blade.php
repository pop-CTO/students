@extends ('layouts.master')
@section('title', "تعديل الالوان ")
@section ('content')

<div class="row">


    <div class="col-md-12">

    <div class="panel panel-inverse" data-sortable-id="form-stuff-3">
        <div class="panel-heading">
            <div class="panel-heading-btn">
            	<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            	<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
            	<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
            </div>
            <h4 class="panel-title"> تعديل الالوان   </h4>
        </div>
        <div class="panel-body">
            <form action="/color/update/{{$mainColor->id}}" method="POST" dir="rtl">
                <!-- // Handel the Cross Site Request Forgery -->
                @csrf
                <input type="hidden" name="id" value="{{$mainColor->id}}">
                <fieldset>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name">اسم الون </label>
                            <input type="text" name="name" class="form-control"  value="{{$mainColor->name}}">
                            @if ($errors->has('name'))
                            <span style="color:red;">{{ $errors->first('name') }}</span>
                            @endif
                        </div>
                    </div><!--end col-md-6-->
                </div><!--end row-->

                <button type="submit" class="btn btn-sm btn-primary m-r-5"> حفظ </button>
                </fieldset>
            </form>
        </div>
    </div>

</div><!-- end col-md-12 -->

</div><!-- end row -->
@endsection