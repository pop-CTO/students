@extends ('layouts.master')
@section('title', "تعديل اعلان")
@section ('content')

<div class="row">


    <div class="col-md-12">

    <div class="panel panel-inverse" data-sortable-id="form-stuff-3">
        <div class="panel-heading">
            <div class="panel-heading-btn">
            	<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            	<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
            	<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
            </div>
            <h4 class="panel-title"> تعديل اعلان</h4>
        </div>
        <div class="panel-body">
                <!--POP Add this to show  sendMessageFalid -->
                @if(Session::get("sendMessageSucc"))
                <<div class="alert alert-success fade in m-b-15 text-center">
                                          <strong>  {{Session::get("sendMessageSucc")}}!</strong>
          
                                          <span class="close" data-dismiss="alert">×</span>
                                      </div>
                @endif
                <!--POP End  -->

                <div class="row">

                    <div class="col-lg-12 text-center">

                            <a href="{{ 'public/'.$ads->photo_url }}" data-lightbox="gallery-group-1">
                                    <img src="{{'public/'.$ads->photo_url }}" style="width: 250px; height: 100px;" >
                              </a>

                        
                    </div>
                </div>
            <form action="/advertisements/updateAdvertisement/{{$ads->id}}" method="POST" dir="rtl" enctype="multipart/form-data">
                <!-- // Handel the Cross Site Request Forgery -->
                @csrf
                <fieldset>
                <div class="row">
                    <legend>تعديل اعلان</legend>

                    <div class="col-md-6">
                    <div class="form-group {{ $errors->has('img_url	') ? ' has-error' : '' }}">
                        <label for="img_url	">صوره الاعلان :</label>
                        <input type="file" name="img_url" class="form-control">
                        
                        @if ($errors->has('img_url	'))
                        <span style="color:red;">{{ $errors->first('img_url	') }}</span>
                        @endif
                    </div>
                    </div><!--end col-md-6-->

                    <div class="col-md-6">
                        <div class="form-group {{ $errors->has('product') ? ' has-error' : '' }}">
                            <label for="link_url">المنتج :</label>
                            <select class="form-control js-example-basic-single" name="product">
                                @if($ads->product_ads !==NULL)
                                    <option value="{{ $ads->prodect_id }}" selected>{{ $ads->product_ads->name }}</option>
                                @endif    
                                    @foreach($product as $products)    
                                            <option value="{{ $products->id }}">{{ $products->name }}</option>
                                            @endforeach
                                          </select>
                        </div>
                    </div><!--end col-md-6-->

                    <div class="form-group{{ $errors->has('is_active') ? ' has-error' : '' }}">
                                        <label for="is_active" class="col-md-2 control-label">وضع التفعيل:</label>

                                        <div class="col-md-8">
                                                <select class="form-control" name="is_active">
                                                  @if($ads->is_active == 0)
                                                    <option value="0">معطل</option>
                                                    <option value="1">مفعل</option>
                                                    @else
                                                      <option value="1">مفعل</option>
                                                      <option value="0">معطل</option>
                                                      @endif
                                                </select>

                                            @if ($errors->has('is_active'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('is_active') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                </div><!--end row-->

                <button type="submit" class="btn btn-primary btn-lg center-block" style="margin-top:10px"> حفظ </button>
                </fieldset>
            </form>
        </div>
    </div>

</div><!-- end col-md-12 -->

</div><!-- end row -->
@endsection
