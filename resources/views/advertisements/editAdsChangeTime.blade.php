@extends ('layouts.master')
@section('title', "تعديل مستخدم")
@section ('content')

<div class="row">


    <div class="col-md-12">

    <div class="panel panel-inverse" data-sortable-id="form-stuff-3">
        <div class="panel-heading">
            <div class="panel-heading-btn">
            	<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            	<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
            	<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
            </div>
            <h4 class="panel-title"> تعديل فتره الاعلان</h4>
        </div>
        <div class="panel-body">
            <form action="/advertisements/update" method="POST" dir="rtl">
                <!-- // Handel the Cross Site Request Forgery -->
                @csrf
                <input type="hidden" name="id" value="csrf_field">
                <fieldset>
                <div class="row">
                    <legend>تعديل فتره الاعلان</legend>

                    <div class="col-md-6">
                    <div class="form-group {{ $errors->has('ads_change_time') ? ' has-error' : '' }}">
                        <label for="ads_change_time">مده الاعلان</label>
                        <input type="text" name="ads_change_time" class="form-control"  value="{{$ads_change_time->ads_change_time}}">
                        @if ($errors->has('ads_change_time'))
                        <span style="color:red;">{{ $errors->first('ads_change_time') }}</span>
                        @endif
                    </div>
                    </div><!--end col-md-6-->


                
                </fieldset>
                <button type="submit" class="btn btn-primary btn-lg center-block" style="margin-top:10px"> حفظ </button>
            </form>
            
        </div>
    </div>

</div><!-- end col-md-12 -->

</div><!-- end row -->
@endsection
