@extends ('layouts.master')
@section('title', "الاعلانات")
@section ('content')
<style>
        .thumbnail .caption {
            padding: 0px;
            color: #333;
        }
</style>
<div class="row">



<div class="col-md-12 col-sm-12">


    <!-- begin panel -->
    <div class="panel panel-inverse">
        <div class="panel-heading">
            <div class="panel-heading-btn">
              <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
              <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
              <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>


            </div>
            <h4 class="panel-title">الاعلانات</h4>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-sm-12 col-md-12">
            @foreach ($uniReq as $UR)
            <div class="thumbnail" style="float:right;margin-left:5px;height: 150px;margin-top:25px;width:250px">
            <a href="{{ 'public/'.$UR->photo_url }}" data-lightbox="gallery-group-1">
                      <img src="{{ 'public/'.$UR->photo_url }}" alt="Card image cap"  style="width: 250px; height: 100px;">
                    </a>
              <div class="text-center caption">
               
                <p>
                <a href="/advertisements/edit/{{$UR->id}}" class="btn btn-success btn-xs"> تعديل </a>
                    <a href="/advertisements/delete/{{$UR->id}}" class="btn btn-danger btn-xs">حذف </a>

                    
                    @if($UR->is_active == 0)
                    <a href="" class="btn btn-danger btn-xs" style="margin-right: 60px">غير مفعل </i>
                    </a>
                    @else
                    <a href="" class="btn btn-success btn-xs" style="margin-right: 60px">مفعل</i>
                    </a>
                    @endif
                
                </p>
              </div>
             
            </div>
            @endforeach 
             <div class="clearfix"></div>
             <div class="container" style="margin-top:25px">
        <a href="{{ route('advertisements.add') }}" class="btn btn-success">انشاء اعلان جديد</i>
        </a>
        </div>
          </div>
        </div>
             
        </div>
    </div>
    <!-- end panel -->
</div>

</div><!-- end row -->
@endsection
