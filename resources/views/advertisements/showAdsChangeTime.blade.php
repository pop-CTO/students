@extends ('layouts.master')
@section('title', "مده الاعلانات")
@section ('content')

<div class="row">

<div class="col-md-12 col-sm-12">

</div>

<div class="col-md-12">


    <!-- begin panel -->
    <div class="panel panel-inverse">
        <div class="panel-heading">
            <div class="panel-heading-btn">
              <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
              <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
              <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>


            </div>
            <h4 class="panel-title"> مده الاعلانات</h4>
        </div>
        <div class="panel-body">

            <table id="data-table" class="table table-striped table-bordered nowrap" width="100%" dir="rtl" >
                <thead>
                    <tr>
                      <th>  المده </th>
                      <th> تعديل</th>

                    </tr>
                </thead>
                <tbody>
                    @foreach ($ads_change_time as $ad)
                    <tr>
                      <td>{{ $ad->ads_change_time }}</td>

                       <td>
                        
                           <a href="/advertisements/editAdsChangeTime" class="btn btn-success btn-md"> تعديل </a>
                       </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <!-- end panel -->
</div>

</div><!-- end row -->
@endsection
