@extends ('layouts.master')
@section('title', "المسئول")
@section ('content')

<div class="row">

<div class="col-md-12 col-sm-12">

</div>

<div class="col-md-12">


    <!-- begin panel -->
    <div class="panel panel-inverse">
        <div class="panel-heading">
            <div class="panel-heading-btn">
              <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
              <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
              <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>


            </div>
            <h4 class="panel-title">المسئول</h4>
        </div>
        <div class="panel-body">
                <!-- begin profile-section -->
                <div class="profile-section">

                    <!-- begin profile-right -->
                    <div class="profile">
                        <!-- begin profile-info -->
                        <div class="profile-info">
                            <!-- begin table -->
                            <div class="table-responsive">
                                <table class="table table-profile">
                                    <thead>
                                        <tr>
                                            <th colspan="2">
                                                <h4>{{$User->name}}</h4>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr class="highlight">
                                            <td class="field">الاسم</td>
                                            <td><a href="#">{{$User->name}}</a></td>
                                        </tr>
                                        <tr class="divider">
                                            <td colspan="2"></td>
                                        </tr>
                                        {{-- <tr>
                                            <td class="field">التليفون</td>
                                            <td><i class="fa fa-mobile fa-lg m-r-5"></i> {{$User->phone}} _ {{$User->codes->number}}</td>
                                        </tr> --}}
                                        {{-- <tr>
                                            <td class="field">كود التليفون</td>
                                            <td><i class="fa fa-mobile fa-lg m-r-5"></i> {{$User->codes->number}}</td>
                                        </tr>                                         --}}
                                        <tr>
                                            <td class="field">الايميل</td>
                                            <td><a href="#">{{$User->email}}</a></td>
                                        </tr>
                                        {{-- <tr class="divider">
                                            <td colspan="2"></td>
                                        </tr> --}}
                                        <tr class="highlight">
                                            <td class="field">الدور</td>
                                            <td>
                                            @if ($User->is_admin === 0) {{'موظف '}} 
                                            @else {{'مسئول'}}
                                            @endif
                                            </td>
                                        </tr>
                                        {{-- <tr class="divider">
                                            <td colspan="2"></td>
                                        </tr> --}}
                                        <tr>
                                            <td class="field">الحالة</td>
                                            <td>
                                            @if ($User->is_active === 0) {{'غير نشط'}} 
                                            @else {{'نشط'}}
                                            @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="field">تاريخ التسجيل</td>
                                            <td>{{date('Y-m-d', strtotime($User->created_at))}}</td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div class="col-lg-12">
                                <a href="users/edit/{{$User->id}}" class="btn btn-success btn-xs"> اضافة  </a>
                                    </div>                               
                            </div>
                            <!-- end table -->
                        </div>
                        <!-- end profile-info -->
                    </div>
                    <!-- end profile-right -->
                </div>
                <!-- end profile-section -->
        </div>
    </div>
    <!-- end panel -->
</div>

</div><!-- end row -->
@endsection