@extends ('layouts.master')
@section('title', "اضافة مسئول")
@section ('content')

<div class="row">


    <div class="col-md-12">

    <div class="panel panel-inverse" data-sortable-id="form-stuff-3">
        <div class="panel-heading">
            <div class="panel-heading-btn">
            	<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            	<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
            	<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
            </div>
            <h4 class="panel-title"> اضافة مسئول </h4>
        </div>
        <div class="panel-body">
            <form action="/user/store" method="POST" dir="rtl" enctype="multipart/form-data">
                <!-- // Handel the Cross Site Request Forgery -->
                @csrf
                <fieldset>
                <div class="row">
                    <legend>اضافة مسئول</legend>

                    <div class="col-md-6">
                    <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                        <label for="name">الاسم</label>
                        <input type="text" name="name" class="form-control"  value="">
                        @if ($errors->has('name'))
                        <span style="color:red;">{{ $errors->first('name') }}</span>
                        @endif
                    </div>
                    </div><!--end col-md-6-->

                    <div class="col-md-6">
                    <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email">الايميل</label>
                        <input type="text" name="email" class="form-control"  value="">
                        @if ($errors->has('email'))
                        <span style="color:red;">{{ $errors->first('email') }}</span>
                        @endif
                    </div>
                    </div><!--end col-md-6-->
                    </div>

                <div class="row">

                    <div class="col-md-6">
                    <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="password">كلمة المرور</label>
                        <input type="password" name="password" class="form-control"  value="">
                        @if ($errors->has('password'))
                        <span style="color:red;">{{ $errors->first('password') }}</span>
                        @endif
                    </div>
                    </div><!--end col-md-6-->

                    <div class="col-md-6">
                        <div class="form-group {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label for="password_confirmation">تأكيد كلمة المرور</label>
                            <input type="password" name="password_confirmation" class="form-control">
                            @if ($errors->has('password_confirmation'))
                            <span style="color:red;">{{ $errors->first('password_confirmation') }}</span>
                            @endif
                        </div>
                    </div><!--end col-md-6-->
                </div>
                <div class="row">

                    <div class="col-md-6">
                    <div class="form-group {{ $errors->has('is_admin') ? ' has-error' : '' }}">
                        <label for='is_admin'>مسئول</label>
                        <select class="form-control" name='is_admin'>
                            <option value="0" >موظف</option>
                            <option value="1" >مسئول</option>
                        </select>
                        @if ($errors->has('is_admin'))
                        <span style="color:red;">{{ $errors->first('is_admin') }}</span>
                        @endif
                    </div>
                    </div><!--end col-md-6-->

                    <div class="col-md-6">
                    <div class="form-group {{ $errors->has('is_active') ? ' has-error' : '' }}">
                        <label for='is_active'>نشط</label>
                        <select class="form-control" name='is_active'>
                            <option value="0" >غير نشط</option>
                            <option value="1" >نشط</option>
                        </select>
                        @if ($errors->has('is_active'))
                        <span style="color:red;">{{ $errors->first('is_active') }}</span>
                        @endif
                    </div>
                    </div><!--end col-md-6-->
                     <div class="col-md-8">
                                        <div class="form-group {{ $errors->has('cities') ? ' has-error' : '' }}">
                                            <label for="cities">اسم المدينه :</label>
                                            <select class="form-control" name='cities'>  
                                                    @foreach ($cities as  $C)
                                                       
                                                            <option selected value="{{$C->id}}">{{$C->name_ar}}</option>
                                                       
                                                        
                                                    @endforeach    
                                            </select>
                                            @if ($errors->has('cities'))
                                            <span style="color:red;">{{ $errors->first('cities') }}</span>
                                            @endif
                                        </div>
                                    </div><!--end col-md-6-->

                </div><!--end row-->

                <button type="submit" class="btn btn-sm btn-primary m-r-5"> حفظ </button>
                </fieldset>
            </form>
        </div>
    </div>

</div><!-- end col-md-12 -->

</div><!-- end row -->
@endsection