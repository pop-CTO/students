@extends ('layouts.master') 
@section('title', "المسؤولين") 
@section ('content')

<div class="row">

    <div class="col-md-12 col-sm-12">

    </div>

    <div class="col-md-12">


        <!-- begin panel -->
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand">
                        <i class="fa fa-expand"></i>
                    </a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload">
                        <i class="fa fa-repeat"></i>
                    </a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse">
                        <i class="fa fa-minus"></i>
                    </a>


                </div>
                <h4 class="panel-title">المسؤولين</h4>
            </div>
            <div class="panel-body">
                @guest
                @else
                @if(Auth::user()->is_admin == 1)
                <h5 class="page-header">
                    <a href="/user/create" class="btn btn-primary btn-xs">اضافة مسئول</a>
                </h5>
                <!-- page-header -->
                @endif 
                @endguest
                <hr />
                <table id="data-table" class="table table-striped table-bordered nowrap" width="100%" dir="rtl">
                    <thead>
                        <tr>
                            <th>الاسم</th>
                            {{--
                            <th>التليفون</th> --}} {{--
                            <th>كود الهاتف</th> --}}
                            <th>الايميل</th>
                            <th>الدور</th>
                            <th>الحالة</th>
                            <th>تاريخ التسجيل</th>

                            @guest
                            @else 
                            @if(Auth::user()->is_admin == 1)
                            <th>تعديل</th>
                            <th>حذف</th>
                            @endif 
                            @endguest
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($users as $U)
                        <tr>
                            <td>
                                <a href="/user/show/{{$U->id}}">{{ $U->name }}</a>
                            </td>
                            {{--
                            <td>{{ $U->phone }}</td> --}} {{--
                            <td>{{ $U->codes->number }}</td> --}}
                            <td>{{ $U->email }}</td>
                            <td>
                                @if ($U->is_admin == 0)
                                <a href="#" class="btn btn-danger btn-xs"> موظف </a>
                                @else
                                <a href="#" class="btn btn-success btn-xs">مسئول</a>
                                @endif
                            </td>
                            <td>
                                @if ($U->is_active == 0)
                                <a href="#" class="btn btn-danger btn-xs">غير نشط</a>
                                @else
                                <a href="#" class="btn btn-success btn-xs">نشط</a>
                                @endif
                            </td>
                            <td>{{ date('Y-m-d', strtotime($U->created_at)) }}</td>

                            @guest
                            @else 
                            @if(Auth::user()->is_admin == 1)
                            <td>
                                <a href="/user/edit/{{$U->id}}" class="btn btn-success btn-xs"> تعديل </a>
                            </td>
                            <td>
                                <a href="/user/destroy/{{$U->id}}" class="btn btn-danger btn-xs"> حذف </a>
                            </td>
                            @endif 
                            @endguest

                        </tr>
                        @endforeach
                    </tbody>
                </table>

            </div>
        </div>
        <!-- end panel -->
    </div>

</div>
<!-- end row -->
@endsection