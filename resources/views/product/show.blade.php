@extends ('layouts.master')
@section('title', "المنتجات ")
@section ('content')

<div class="row">

<div class="col-md-12 col-sm-12">

</div>

<div class="col-md-12">


    <!-- begin panel -->
    <div class="panel panel-inverse">
        <div class="panel-heading">
            <div class="panel-heading-btn">
              <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
              <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
              <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>


            </div>
            <h4 class="panel-title">المنتج</h4>
        </div>
        <div class="panel-body">
                <!-- begin profile-section -->
                <div class="profile-section">

                    <!-- begin profile-right -->
                    <div class="profile">
                        <!-- begin profile-info -->
                        <div class="profile-info">
                            <!-- begin table -->
                            <div class="table-responsive">
                                <table class="table table-profile">
                                    <thead>
                                        <tr>
                                            <th colspan="2">
                                                <h4>{{$product->name}}</h4>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr class="highlight">
                                            <td class="field">اسم المنتج </td>
                                            <td><a href="#">{{$product->name}}</a></td>
                                        </tr>


                                    
                                            
                                            
                                        <tr>
                                            <td class="highlight">سعر المنتج </td>
                                            <td> {{$product->price}}</td>
                                        </tr>

                                        <tr class="divider">
                                            <td colspan="2"></td>
                                        </tr>
                                        
                                        <tr>
                                            <td class="highlight">الكمية المتاحة </td>
                                            <td><a href="#">{{$product->mount}}</a></td>
                                        </tr>
                                        

                                        <tr>
                                                <td class="highlight">وصف المنتج  </td>
                                                <td> {{ $product->description }}</td>
                                            </tr>
    
                                            
                                            <tr class="divider">
                                                <td colspan="2"></td>
                                            </tr>
                                            
                                            <tr>
                                                    <td class="field">القسم </td>
                                                    <td>{{$product->product_type->name}}</td>
                                                    </tr>
                                            <tr>
                                                    <tr>
                                                            <td class="field">العلامة التجارية </td>
                                                            <td>{{$product->product_brand->name}}</td>
                                                            </tr>
                                                    <tr>

                                                    <tr class="divider">
                                                            <td colspan="2"></td>
                                                        </tr>
                                            <td class="field">تاريخ التسجيل</td>
                                            <td>{{$product->created_at}}</td>
                                            </tr>
                                            <tr class="divider">
                                                    <td colspan="2"></td>
                                                </tr>
                                            <tr>

                                            <td class="field">عدد المشاهدة </td>
                                            <td>{{$product->n_views}}</td>
                                        </tr>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!-- end table -->
                        </div>
                        <!-- end profile-info -->
                    </div>
                    <!-- end profile-right -->
                </div>
<div class="clearfix"></div>
             <div class="container" style="margin-top:25px">
                 
        <a href="{{ route('product.destroy',$product->id ) }}" class="btn btn-danger">حذف المنتج </a>   </i>
        </a>
        </div>                <!-- end profile-section -->
        </div>
    </div>
    <!-- end panel -->
</div>




<div class="col-md-12">


        <!-- begin panel -->
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                  <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                  <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                  <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
    
    
                </div>
                <h4 class="panel-title">صور المنتج </h4>
            </div>
            <div class="panel-body">
                    <!-- begin profile-section -->
                    <div class="profile-section">
    
                        <!-- begin profile-right -->
                        <div class="profile">
                            <!-- begin profile-info -->
                            <div class="profile-info">
                                    <div class="row">
                                        @foreach ($images as $image)
                                            
                                            <div class="col-sm-6 col-md-4">
                                              <div class="thumbnail">
                                                <img src="{{ url('/public').'/'.$image->url }}" alt="...">
                                                <div class="caption">
                                                </div>
                                              </div>
                                            </div>
                                        @endforeach

                            </div>
                            <!-- end profile-info -->
                        </div>
                        <!-- end profile-right -->
                    </div>
        </div>
        <!-- end panel -->
    </div>
        </div>
    </div>

    <div class="col-md-12">


        <!-- begin panel -->
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                  <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                  <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                  <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
    
    
                </div>
                <h4 class="panel-title">الالوان </h4>
            </div>
            <div class="panel-body">
                    <!-- begin profile-section -->
                    <div class="profile-section">
    
                        <!-- begin profile-right -->
                        <div class="profile">
                            <!-- begin profile-info -->
                            <div class="profile-info">
                                    <div class="row">
                                        <table id="data-table" class="table table-striped table-bordered nowrap" width="100%" dir="rtl" >
                                            <thead>
                                                <tr>
                                                  <th>الاسم</th>
                                                  <th>حذف</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($product->product_color as $colors)
                                                <tr>
                                                  <td>{{ $colors->name }}</td>
                                                  <td><a href="/product/color/destroy/{{$colors->id}}" class="btn btn-danger btn-xs"> حذف </a></td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>

                            </div>
                            <div class="row">
                <!--POP End  -->
                            <form action="{{route('product.color.add',$product->id)}}" method="get" dir="rtl" enctype="multipart/form-data">
                    <!-- // Handel the Cross Site Request Forgery -->
                    @csrf
                    <fieldset>
                        <div class="col-md-6">
                            <div class="form-group {{ $errors->has('color') ? ' has-error' : '' }}">
                                    <label for="color"> الالوان   </label>
                                   <select class="js-example-basic-multiple" multiple="multiple" name="color[]">
                                       @foreach($color as $colors)    
                                            <option value="{{ $colors->id }}">{{ $colors->name }}</option>
                                            @endforeach
                                          </select>
                                          @if ($errors->has('color'))
                                          <span style="color:red;">{{ $errors->first('color') }}</span>
                                          @endif                                             
                        </div>
                            </div>
        
                            
                        
                    </div><!--end col-md-6-->
                    <button type="submit" class="btn btn-sm btn-primary m-r-5"> حفظ </button>
                    </fieldset>
                </form>
                                </div>
                            </div>
                            <!-- end profile-info -->
                        </div>
                        <!-- end profile-right -->
                    </div>
        </div>
        </div>
        <!-- end panel -->
    </div>
</div><!-- end row -->
@endsection