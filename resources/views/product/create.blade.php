@extends ('layouts.master')
@section('title', "اضافه منتج")
@section ('content')

<div class="row">


    <div class="col-md-12">

    <div class="panel panel-inverse" data-sortable-id="form-stuff-3">
        <div class="panel-heading">
            <div class="panel-heading-btn">
            	<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            	<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
            	<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
            </div>
            <h4 class="panel-title"> اضافة منتج </h4>
        </div>
        <div class="panel-body">
                <!--POP Add this to show  sendMessageFalid -->
                @if(Session::get("sendMessageSucc"))
                <<div class="alert alert-success fade in m-b-15 text-center">
                                          <strong>  {{Session::get("sendMessageSucc")}}!</strong>
          
                                          <span class="close" data-dismiss="alert">×</span>
                                      </div>
                @endif
                <!--POP End  -->
            <form action="store" method="POST" dir="rtl" enctype="multipart/form-data">
                <!-- // Handel the Cross Site Request Forgery -->
                @csrf
                <fieldset>
                <div class="row">
                    <legend>اضافة منتج</legend>
                    <div class="row">
                    <div class="col-md-6">
                    <div class="form-group {{ $errors->has('img_url	') ? ' has-error' : '' }}">
                        <label for="img_url	">صوره منتج </label>
                        <input type="file" name="img_url[]" class="form-control" multiple>
                        
                        @if ($errors->has('img_url'))
                        <span style="color:red;">{{ $errors->first('img_url') }}</span>
                        @endif
                    </div>
                    </div><!--end col-md-6-->
                

                    <div class="col-md-6">
                        <div class="form-group {{ $errors->has('productName') ? ' has-error' : '' }}">
                                <label for="img_url	">اسم المنتج </label>
                                <input type="text"  class="form-control" name="productName" placeholder="" value="{{ old('productName') }}">
                                @if ($errors->has('productName'))
                                <span style="color:red;">{{ $errors->first('productName') }}</span>
                                @endif
                        </div>
                    </div><!--end col-md-6-->
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group {{ $errors->has('price') ? ' has-error' : '' }}">
                                <label for="img_url	">سعر المنتج  </label>
                                <input type="number"  class="form-control" name="price" placeholder="" value="{{ old('price') }}">
                                @if ($errors->has('price'))
                                <span style="color:red;">{{ $errors->first('price') }}</span>
                                @endif
                        </div>
                    </div><!--end col-md-6-->


                    <div class="col-md-6">
                        <div class="form-group {{ $errors->has('mount') ? ' has-error' : '' }}">
                                <label for="img_url	">الكمية المتاحة </label>
                                <input type="number"  class="form-control" name="mount" placeholder="" value="{{ old('mount') }}">
                                @if ($errors->has('mount'))
                                <span style="color:red;">{{ $errors->first('mount') }}</span>
                                @endif
                        </div>
                    </div><!--end col-md-6-->
                    </div>
                    <div class="row">
                    <div class="col-md-6">
                        <div class="form-group {{ $errors->has('discount') ? ' has-error' : '' }}">
                                <label for="img_url	">الخصم  </label>
                                <input type="number"  class="form-control" name="discount" placeholder="%" value="{{ old('discount') }}">
                                @if ($errors->has('discount'))
                                <span style="color:red;">{{ $errors->first('discount') }}</span>
                                @endif   
                        </div>
                    </div>


                        <div class="col-md-6">
                            <div class="form-group {{ $errors->has('Type') ? ' has-error' : '' }}">
                                    <label for="img_url	"> الاقسام </label>
                                   <select class="form-control js-example-basic-single" name="Type">
                                       @foreach($Type as $Types)    
                                            <option value="{{ $Types->id }}">{{ $Types->name }}</option>
                                            @endforeach
                                          </select>
                                          @if ($errors->has('Type'))
                                          <span style="color:red;">{{ $errors->first('Type') }}</span>
                                          @endif                                             
                        </div>

                            
                        
                    </div><!--end col-md-6-->
                    </div>

                    <div class="row">
                    <div class="col-md-6">
                        <div class="form-group {{ $errors->has('brand') ? ' has-error' : '' }}">
                                <label for="brand	"> العلامة التجارية  </label>
                               <select class="form-control js-example-basic-single" name="brand">
                                   @foreach($brand as $brands)    
                                        <option value="{{ $brands->id }}">{{ $brands->name }}</option>
                                        @endforeach
                                      </select>
                                      @if ($errors->has('brand'))
                                      <span style="color:red;">{{ $errors->first('brand') }}</span>
                                      @endif                                             
                    </div>
                       

                        
                    
                </div><!--end col-md-6-->

                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('color') ? ' has-error' : '' }}">
                            <label for="color"> الالوان   </label>
                           <select class="form-control js-example-basic-multiple" multiple="multiple" name="color[]">
                               @foreach($color as $colors)    
                                    <option value="{{ $colors->id }}">{{ $colors->name }}</option>
                                    @endforeach
                                  </select>
                                  @if ($errors->has('color'))
                                  <span style="color:red;">{{ $errors->first('color') }}</span>
                                  @endif                                             
                </div>
                    </div>
                </div>

                    
                    <div class="row">                
                    <div class="col-md-12">
                            <div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
                                    <label for="img_url	">وصف  المنتج </label>
                                    <textarea rows="4" cols="50"  class="form-control" name="description" placeholder="" value="{{ old('description') }}">
                                    </textarea>
                                    @if ($errors->has('description'))
                                    <span style="color:red;">{{ $errors->first('description') }}</span>
                                    @endif                                
                            </div>
                        </div><!--end col-md-6-->
                    </div>

                    
                    
                </div><!--end row-->

                <button type="submit" class="btn btn-sm btn-primary m-r-5"> حفظ </button>
                </fieldset>
            </form>
        </div>
   

</div><!-- end col-md-12 -->

</div><!-- end row -->
@endsection
