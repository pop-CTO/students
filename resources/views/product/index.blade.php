@extends ('layouts.master')
@section('title', "المنتجات")
@section ('content')
<style>
        body {
            padding-top: 70px;}
          .row .flex {
           display: inline-flex;
           width: 100%;}

          img {
            max-width: 60%;
            height: 425px}

            .carousel-inner>.item>a>img, .carousel-inner>.item>img, .img-responsive, .thumbnail a>img, .thumbnail>img {
                display: block;
                max-width: 60%;
                height: 425px}
</style>
<div class="row">



<div class="col-md-12 col-sm-12">


    <!-- begin panel -->
    <div class="panel panel-inverse">
        <div class="panel-heading">
            <div class="panel-heading-btn">
              <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
              <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
              <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>


            </div>
            <h4 class="panel-title">المنتجات</h4>
        </div>
        <div class="panel-body">
            <div class="row">

            @foreach ($uniReq as $UR)
            <div class="col-sm-6 col-md-4">

                  <div class="thumbnail ">
                      
                                            
                 
                             @foreach ($UR->product_images as $image)    
                             @if ($loop->first)
                     
                                    <img src="{{ url('/public').'/'.$image->url }}" class="portrait " alt="Los Angeles">
                
                             @endif
                             @endforeach  


                    <div class="caption text-center">
                      <h3>{{ $UR->name }}</h3>
                      <p>{!! Str::words($UR->description, 5) !!}</p>
                      <p>
                          <a href="product/edit/{{ $UR->id }}" class="btn btn-primary" role="button">تعديل </a> 
                          <a href="product/show/{{ $UR->id }}" class="btn btn-default" role="button">عرض</a></p>

                    </div>
                  </div>
                </div>        
            @endforeach 
             <div class="clearfix"></div>
             <div class="container" style="margin-top:25px">
        <a href="{{ route('product.add') }}" class="btn btn-success">اضافة منتج جديد</i>
        </a>
        </div>
          </div>
        </div>
             
        </div>
    </div>
    <!-- end panel -->
</div>

</div><!-- end row -->
@endsection
