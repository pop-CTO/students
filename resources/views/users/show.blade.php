@extends ('layouts.master')
@section('title', "المستخدم")
@section ('content')

<div class="row">

<div class="col-md-12 col-sm-12">

</div>

<div class="col-md-12">


    <!-- begin panel -->
    <div class="panel panel-inverse">
        <div class="panel-heading">
            <div class="panel-heading-btn">
              <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
              <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
              <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>


            </div>
            <h4 class="panel-title">المستخدم</h4>
        </div>
        <div class="panel-body">
                @if ($User->deleted_at !== null)
                <div class="row">
                        <div class="col-lg-12">
                                <div class="alert alert-danger text-center">
                                        <strong>تم حذف حساب المستخدم </strong>
                                      </div>
                        </div>
                    </div>
                @endif
            
                <!-- begin profile-section -->
                <div class="profile-section">

                    <!-- begin profile-right -->
                    <div class="profile">
                        <!-- begin profile-info -->
                        <div class="profile-info">
                            <!-- begin table -->
                            <div class="table-responsive">
                                <table class="table table-profile">
                                    <thead>
                                        <tr>
                                            <th colspan="2">
                                                <h4>{{$User->name}}</h4>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr class="highlight">
                                            <td class="field">الاسم</td>
                                            <td><a href="#">{{$User->name}}</a></td>
                                        </tr>


                                        <tr class="highlight">
                                                <td class="field">الصورة الشخصية </td>
                                                <td style="
                                                text-align:  center;
                                            "><img style="width: 18%;" src={{ url('public').'/'.$User->photo_url}}></td>
                                            </tr>
    
                                            <tr class="divider">
                                                <td colspan="2"></td>
                                            </tr>
                                            
                                            
                                        <tr>
                                            <td class="highlight">التليفون</td>
                                            <td><i class="fa fa-mobile fa-lg m-r-5"></i> {{$User->phone}}</td>
                                        </tr>

                                        <tr class="divider">
                                            <td colspan="2"></td>
                                        </tr>
                                        <tr>
                                                <td class="highlight">المدينة </td>
                                                <td> {{ $User->city}}</td>
                                            </tr>
    
                                            <tr class="divider">
                                                <td colspan="2"></td>
                                            </tr>
                                            
                                      
                                                    <tr>
                                                            <td class="field">محافظة  </td>
                                                            <td> {{ $User->area }}</td>
                                                        </tr>
                                        <tr>
                                                <td class="highlight">منطقة</td>
                                                <td> {{$User->district ==NULL?"لايوجد" :$User->district }}</td>
                                            </tr>
    
                                            <tr class="divider">
                                                <td colspan="2"></td>
                                            </tr>
                                            <tr>
                                                    <td class="highlight">الشارع </td>
                                                    <td> {{$User->street ==NULL?"لايوجد"  :$User->street}}</td>
                                                </tr>
        
                                                <tr class="divider">
                                                    <td colspan="2"></td>
                                                </tr>
                                                <tr>
                                                        <td class="highlight">رقم المنزل</td>
                                                        <td> {{$User->home ==NULL?"لايوجد" :$User->home }}</td>
                                                    </tr>
            
                                                    <tr class="divider">
                                                        <td colspan="2"></td>
                                                    </tr>                                        
                                        
                                        <tr>
                                            <td class="highlight">الايميل</td>
                                            <td><a href="#">{{$User->email}}</a></td>
                                        </tr>
                                        



                                        <tr class="highlight">
                                            <td class="field">مفعل</td>
                                            <td><a href="#">
                                            @if ($User->is_valid === 0) {{'غير مفعل'}} 
                                            @else {{'مفعل'}}
                                            @endif
                                            </a></td>
                                        </tr>
                                        <tr>
                                            <td class="field">الحالة</td>
                                            <td>
                                            @if ($User->is_active === 0) {{'غير نشط'}} 
                                            @else {{'نشط'}}
                                            @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="field">تاريخ التسجيل</td>
                                            <td>{{$User->created_at}}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!-- end table -->
                        </div>
                        <!-- end profile-info -->
                    </div>
                    <!-- end profile-right -->
                </div>
                <!-- end profile-section -->
        </div>
    </div>
    <!-- end panel -->
</div>

</div><!-- end row -->
@endsection