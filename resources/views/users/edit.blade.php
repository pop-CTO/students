@extends ('layouts.master')
@section('title', "تعديل مستخدم")
@section ('content')

<div class="row">


    <div class="col-md-12">

    <div class="panel panel-inverse" data-sortable-id="form-stuff-3">
        <div class="panel-heading">
            <div class="panel-heading-btn">
            	<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            	<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
            	<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
            </div>
            <h4 class="panel-title"> تعديل مستخدم </h4>
        </div>
        <div class="panel-body">
                <!--POP Add this to show  sendMessageFalid -->
                @if(Session::get("sendMessageSucc"))
                <<div class="alert alert-success fade in m-b-15 text-center">
                                          <strong>  {{Session::get("sendMessageSucc")}}!</strong>
          
                                          <span class="close" data-dismiss="alert">×</span>
                                      </div>
                @endif
                <!--POP End  -->
            <form action="/users/update/{{$User->id}}" method="POST" dir="rtl" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="id" value="{{$User->id}}">
                <fieldset>
                <div class="row">
                    <legend>تعديل مستخدم</legend>

                    <div class="col-md-6">
                    <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                        <label for="name">الاسم</label>
                        <input type="text" name="name" class="form-control"  value="{{$User->name}}">
                        @if ($errors->has('name'))
                        <span style="color:red;">{{ $errors->first('name') }}</span>
                        @endif
                    </div>
                    </div><!--end col-md-6-->

                    <div class="col-md-6">
                    <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email">الايميل</label>
                        <input type="text" name="email" class="form-control"  value="{{$User->email}}">
                        @if ($errors->has('email'))
                        <span style="color:red;">{{ $errors->first('email') }}</span>
                        @endif
                    </div>
                    </div><!--end col-md-6-->
                    </div>

                    <div class="row">

                    <div class="col-md-6">
                        <div class="form-group {{ $errors->has('phone') ? ' has-error' : '' }}">
                            <label for="phone">رقم التليفون</label>
                            <input type="name" name="phone" class="form-control"  value="{{$User->phone}}">
                            @if ($errors->has('phone'))
                            <span style="color:red;">{{ $errors->first('phone') }}</span>
                            @endif
                        </div>
                    </div><!--end col-md-6-->

                    <div class="col-md-6">
                        <div class="form-group {{ $errors->has('is_active') ? ' has-error' : '' }}">
                            <label for='is_active'>نشط</label>
                            <select class="form-control" name='is_active'>
                                <option value="0" @if ($User->is_active === 0) {{'selected'}} @endif>غير نشط</option>
                                <option value="1" @if ($User->is_active === 1) {{'selected'}} @endif>نشط</option>
                            </select>
                            @if ($errors->has('is_active'))
                            <span style="color:red;">{{ $errors->first('is_active') }}</span>
                            @endif
                        </div>
                        </div><!--end col-md-6-->                    
                </div>


                <div class="row">

                    <div class="col-md-6">
                    <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="password">كلمة المرور</label>
                        <input type="password" name="password" class="form-control"  value="">
                        @if ($errors->has('password'))
                        <span style="color:red;">{{ $errors->first('password') }}</span>
                        @endif
                    </div>
                    </div><!--end col-md-6-->

                    <div class="col-md-6">
                        <div class="form-group {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label for="password_confirmation">تأكيد كلمة المرور</label>
                            <input type="password" name="password_confirmation" class="form-control">
                            @if ($errors->has('password_confirmation'))
                            <span style="color:red;">{{ $errors->first('password_confirmation') }}</span>
                            @endif
                        </div>
                    </div><!--end col-md-6-->
                </div>

                <div class="row">

                    <div class="col-md-6">
                        <div class="form-group {{ $errors->has('city_id') ? ' has-error' : '' }}">
                            <label for='city_id'>المدينة </label>
                            <select class="form-control" name='city_id'>
                                <option value="{{ $User->city_id }}" selected>{{ $User->user_city->name }}</option>
                            @foreach($City as $city)
                                <option value="{{ $city->id }}">{{ $city->name }}</option>
                            @endforeach    
                            </select>
                            @if ($errors->has('city_id'))
                            <span style="color:red;">{{ $errors->first('city_id') }}</span>
                            @endif
                        </div>
                        </div><!--end col-md-6-->        

                    <div class="col-md-6">
                        <div class="form-group {{ $errors->has('area_id') ? ' has-error' : '' }}">
                            <label for='area_id'>القطاع  </label>
                            <select class="form-control" name='area_id'>
                                <option value="{{ $User->area_id }}" selected>{{ $User->user_area->name }}</option>
                            @foreach($Area as $area)
                                <option value="{{ $area->id }}">{{ $area->name }}</option>
                            @endforeach    
                            </select>
                            @if ($errors->has('area_id'))
                            <span style="color:red;">{{ $errors->first('area_id') }}</span>
                            @endif
                        </div>
                    </div><!--end col-md-6-->
                </div>
                <div class="row">     
                    <div class="col-md-6">
                        <div class="form-group {{ $errors->has('area_id') ? ' has-error' : '' }}">
                            <label for='area_id'>صورة شخصية   </label>
                            <input type="file" name="photoUrl" class="form-control">
                            @if ($errors->has('area_id'))
                            <span style="color:red;">{{ $errors->first('area_id') }}</span>
                            @endif
                        </div>
                    </div><!--end col-md-6-->
                </div>                

                <div class="row">

                        <div class="col-md-6">
                            <div class="form-group {{ $errors->has('home') ? ' has-error' : '' }}">
                                <label for="home">رقم المنزل</label>
                                <input type="name" name="home" class="form-control"  value="{{$User->home}}">
                                @if ($errors->has('home'))
                                <span style="color:red;">{{ $errors->first('home') }}</span>
                                @endif
                            </div>
                        </div><!--end col-md-6-->

                        <div class="col-md-6">
                                <div class="form-group {{ $errors->has('street') ? ' has-error' : '' }}">
                                    <label for="street">الشارع </label>
                                    <input type="name" name="street" class="form-control"  value="{{$User->street}}">
                                    @if ($errors->has('street'))
                                    <span style="color:red;">{{ $errors->first('street') }}</span>
                                    @endif
                                </div>
                            </div><!--end col-md-6-->                     
                    </div>                


                    <div class="row">

                            <div class="col-md-6">
                                <div class="form-group {{ $errors->has('district') ? ' has-error' : '' }}">
                                    <label for="district">المنطقة  </label>
                                    <input type="name" name="district" class="form-control"  value="{{$User->district}}">
                                    @if ($errors->has('district'))
                                    <span style="color:red;">{{ $errors->first('district') }}</span>
                                    @endif
                                </div>
                            </div><!--end col-md-6-->
                 
                        </div>                     
                <button type="submit" class="btn btn-sm btn-primary m-r-5"> حفظ </button>
                </fieldset>
            </form>
        </div>
    </div>

</div><!-- end col-md-12 -->

</div><!-- end row -->
@endsection