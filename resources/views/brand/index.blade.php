@extends ('layouts.master')
@section('title', "العلامات التجارية ")
@section ('content')

<div class="row">

<div class="col-md-12 col-sm-12">

</div>

<div class="col-md-12">


    <!-- begin panel -->
    <div class="panel panel-inverse">
        <div class="panel-heading">
            <div class="panel-heading-btn">
              <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
              <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
              <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>


            </div>
            <h4 class="panel-title">العلامات التجارية</h4>
        </div>
        <div class="panel-body">
                <!--POP Add this to show  sendMessageFalid -->
                @if(Session::get("sendMessageSucc"))
                <div class="alert alert-success fade in m-b-15 text-center">
                                          <strong>  {{Session::get("sendMessageSucc")}}!</strong>
          
                                          <span class="close" data-dismiss="alert">×</span>
                                      </div>
                @endif
                <!--POP End  -->
            <table class="table table-striped table-bordered nowrap" width="100%" dir="rtl" >
                <thead>
                    <tr>
                      <th>الاسماء</th>
                      <th>تعديل</th>
                      <th>حذف</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($brands as $brand)
                    <tr>
                      <td>{{ $brand->name }}</td>
                      <td><a href="/brand/edit/{{$brand->id}}" class="btn btn-success btn-xs"> تعديل </a></td>
                      <td><a href="/brand/destroy/{{$brand->id}}" class="btn btn-danger btn-xs"> حذف </a></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="col-lg-12">
                    <a href="/brand/create" class="btn btn-success btn-xs"> اضافة  </a>
                </div>
        </div>
    </div>

    <!-- end panel -->
</div>



</div><!-- end row -->
@endsection