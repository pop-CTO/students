@extends ('layouts.master')
@section('title', "المستخدم")
@section ('content')

<div class="row">

<div class="col-md-12 col-sm-12">

</div>

<div class="col-md-12">


    <!-- begin panel -->
    <div class="panel panel-inverse">
        <div class="panel-heading">
            <div class="panel-heading-btn">
              <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
              <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
              <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>


            </div>
            <h4 class="panel-title">الرسالة</h4>
        </div>

        <div class="panel panel-success" data-sortable-id="ui-widget-16">

            <div class="panel-body bg-green text-white">
                <p>{{$message->message}}</p>
            </div>
        </div>

    </div>
    <!-- end panel -->
</div>

</div><!-- end row -->
@endsection