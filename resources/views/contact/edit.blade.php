@extends ('layouts.master')
@section('title', "تعديل بيانات التواصل")
@section ('content')

<div class="row">


    <div class="col-md-12">

    <div class="panel panel-inverse" data-sortable-id="form-stuff-3">
        <div class="panel-heading">
            <div class="panel-heading-btn">
            	<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            	<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
            	<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
            </div>
            <h4 class="panel-title"> تعديل  </h4>
        </div>
        <div class="panel-body">
            <form action="/contact/update/{{$contact->id}}" method="POST" dir="rtl">
                <!-- // Handel the Cross Site Request Forgery -->
                @csrf
                <input type="hidden" name="id" value="{{$contact->id}}">
                <fieldset>
                <div class="row">

                    <div class="col-md-6">
                        <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email">الايميل</label>
                            <input type="text" name="email" class="form-control"  value="{{$contact->email}}">
                            @if ($errors->has('email'))
                            <span style="color:red;">{{ $errors->first('email') }}</span>
                            @endif
                        </div>
                    </div><!--end col-md-6-->

                    <div class="col-md-6">
                        <div class="form-group {{ $errors->has('phone') ? ' has-error' : '' }}">
                            <label for="phone">رقم التليفون</label>
                            <input type="name" name="phone" class="form-control"  value="{{$contact->phone}}">
                            @if ($errors->has('phone'))
                            <span style="color:red;">{{ $errors->first('phone') }}</span>
                            @endif
                        </div>
                    </div><!--end col-md-6-->

                </div><!--end row-->

                <button type="submit" class="btn btn-sm btn-primary m-r-5"> حفظ </button>
                </fieldset>
            </form>
        </div>
    </div>

</div><!-- end col-md-12 -->

</div><!-- end row -->
@endsection