@extends ('layouts.master')
@section('title', "سياسات الموقع")
@section ('content')

<div class="row">

<div class="col-md-12 col-sm-12">

</div>

<div class="col-md-12">


    <!-- begin panel -->
    <div class="panel panel-inverse">
        <div class="panel-heading">
            <div class="panel-heading-btn">
              <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
              <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
              <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>


            </div>
            <h4 class="panel-title">سياسات التطبيق</h4>
        </div>
        <div class="panel-body">

            <table class="table table-striped table-bordered nowrap" width="100%" dir="rtl" >
                <thead>
                    <tr>
                      <th>سياسة التطبيق </th>
                      {{-- <th>الايميل</th> --}}
                      {{-- <th>تاريخ الرسالة</th> --}}
                      <th>تعديل</th>
                      {{-- <th>حذف</th> --}}
                    </tr>
                </thead>
                <tbody>
                    @foreach ($policies as $policy)
                    <tr>
                      <td><a href="/policy/show/{{$policy->id}}">{{ str_limit($policy->police_terms, 200) }}</a></td>
                      {{-- <td>{{ $policy->email }}</td> --}}
                      {{-- <td>{{ $policy->created_at }}</td> --}}
                      <td><a href="/policy/edit/{{$policy->id}}" class="btn btn-success btn-xs"> تعديل </a></td>
                      {{-- <td><a href="/policy/destroy/{{$policy->id}}" class="btn btn-danger btn-xs"> حذف </a></td> --}}
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <!-- end panel -->
</div>

</div><!-- end row -->
@endsection