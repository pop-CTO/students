@extends ('layouts.master')
@section('title', "تعديل مستخدم")
@section ('content')

<div class="row">


    <div class="col-md-12">

    <div class="panel panel-inverse" data-sortable-id="form-stuff-3">
        <div class="panel-heading">
            <div class="panel-heading-btn">
            	<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            	<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
            	<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
            </div>
            <h4 class="panel-title"> تعديل البنود </h4>
        </div>
        <div class="panel-body">
            <form action="/policy/update/{{$policies->id}}" method="POST" dir="rtl">
                <!-- // Handel the Cross Site Request Forgery -->
                @csrf
                <fieldset>
                <div class="row">

                    <div class="col-md-12">
                        <div class="form-group {{ $errors->has('policy_terms_ar') ? ' has-error' : '' }}">
                            <label for="policy_terms_ar">سياسة التطبيق</label>
                            <textarea name="policy_terms_ar" class="form-control" rows="5">{{$policies->police_terms}}</textarea>
                            @if ($errors->has('policy_terms_ar'))
                            <span style="color:red;">{{ $errors->first('policy_terms_ar') }}</span>
                            @endif
                        </div>
                    </div><!--end col-md-6-->

                </div><!--end row-->

                <button type="submit" class="btn btn-sm btn-primary m-r-5"> حفظ </button>
                </fieldset>
            </form>
        </div>
    </div>

</div><!-- end col-md-12 -->

</div><!-- end row -->
@endsection