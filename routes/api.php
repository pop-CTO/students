<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post("policyTerms","AppSittengs@App_sitteng");

Route::post("search","products@search");



Route::post("productInfo","products@productsInfo");
Route::post("accounts","AppSittengs@accounts");
// Route::post("cities","CityConttroller@Cites");
Route::post("register","UsersController@register");
Route::post("validateCode","forgetPassword@validateCode");
Route::post("forgetPassword","forgetPassword@forgetPassword");
Route::post("resendCode","forgetPassword@resendCode");
Route::post("changePassword","forgetPassword@changePassword");
Route::post("login","login@login");
Route::post("updateProfile","updateProfile@updateProfile");
Route::post("updatePassword","updateProfile@updatePassword");
Route::post("products","products@products");
Route::post("ads","Ads@Ads");
Route::post("productClick","products@productClick");
Route::post("Product/mostView","products@mostView");
Route::post("aboutUs","AppSittengs@aboutUs");
Route::post("notificationRegistration","UsersController@notificationRegistration");

Route::post("addOrder","Orders@addOrder");

Route::post("getOrder","Orders@getOrder");

Route::post("deleteOrder","Orders@deleteOrder");

Route::post("addBilles","Orders@addBilles");

Route::post("getBilles","Orders@getBilles");

Route::post("updateOrder","Orders@updateOrder");

Route::post("color","products@color");
Route::post("brand","products@brand");
Route::post("visitor","UsersController@visitor");

