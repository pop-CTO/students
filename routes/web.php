<?php


Route::any('register','Admin\AppsettingController@index');
Auth::routes();

Route::group(['middleware' => 'auth'], function()
{
  
    Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

    Route::get('users','Admin\UserController@index');
    Route::post('users/update/{id}','Admin\UserController@update');
    Route::get('users/show/{id}','Admin\UserController@show');
    Route::get('users/edit/{id}','Admin\UserController@edit');
    Route::get('users/destroy/{id}','Admin\UserController@destroy');
    
    Route::get('advertisements','Admin\AdsController@index');
    Route::get('advertisements/create','Admin\AdsController@create')->name('advertisements.add');
    Route::get('advertisements/show/{id}','Admin\AdsController@show');
    Route::post('advertisements/store','Admin\AdsController@store');
    Route::get('advertisements/edit/{id}','Admin\AdsController@edit');
    Route::get('advertisements/delete/{id}','Admin\AdsController@destroy');
    Route::post('advertisements/updateAdvertisement/{id}','Admin\AdsController@update');
    

    Route::get('orders','Admin\OrderController@index');
    Route::get('orders/show/{id}','Admin\OrderController@show');
    Route::get('orders/done/{id}','Admin\OrderController@done');
    Route::get('orders/finsih/{id}','Admin\OrderController@finish');
    Route::get('orders/destroy/{id}','Admin\OrderController@destroy');
    
    
    // Route::get('area','Admin\AreaController@index');
    // Route::post('area/update/{id}','Admin\AreaController@update');
    // Route::get('area/create','Admin\AreaController@create');
    // Route::post('area/store','Admin\AreaController@store');
    // Route::get('area/edit/{id}','Admin\AreaController@edit');
    // Route::get('area/destroy/{id}','Admin\AreaController@destroy');
    
    Route::get('policy','Admin\policyController@index');
    Route::post('policy/update/{id}','Admin\policyController@update');
    Route::get('policy/edit/{id}','Admin\policyController@edit');
    Route::get('policy/show/{id}','Admin\policyController@show');
    

    Route::get('about','Admin\aboutController@index');
    Route::post('about/update/{id}','Admin\aboutController@update');
    Route::get('about/edit/{id}','Admin\aboutController@edit');
    Route::get('about/show/{id}','Admin\aboutController@show');
    
    Route::get('contact','Admin\contactController@index');
    Route::post('contact/update/{id}','Admin\contactController@update');
    Route::get('contact/edit/{id}','Admin\contactController@edit');
    Route::get('contact/show/{id}','Admin\contactController@show');
    

    Route::get('color','Admin\mainColorController@index');
    Route::get('color/create','Admin\mainColorController@create');
    Route::post('color/update/{id}','Admin\mainColorController@update');
    Route::post('color/store','Admin\mainColorController@store');
    Route::get('color/edit/{id}','Admin\mainColorController@edit');
    Route::get('color/show/{id}','Admin\mainColorController@show');
    Route::get('color/destroy/{id}','Admin\mainColorController@destroy');

    // Route::get('city','Admin\CityController@index');
    // Route::get('city/create','Admin\CityController@create');
    // Route::post('city/update/{id}','Admin\CityController@update');
    // Route::post('city/store','Admin\CityController@store');
    // Route::get('city/edit/{id}','Admin\CityController@edit');
    // Route::get('city/show/{id}','Admin\CityController@show');


    Route::get('brand','Admin\brandController@index');
    Route::get('brand/create','Admin\brandController@create');
    Route::post('brand/update/{id}','Admin\brandController@update');
    Route::post('brand/store','Admin\brandController@store');
    Route::get('brand/edit/{id}','Admin\brandController@edit');
    Route::get('brand/show/{id}','Admin\brandController@show');
    Route::get('brand/destroy/{id}','Admin\brandController@destroy');


    Route::post('logout', ['as' => 'logout', 'uses' => 'Auth\LoginController@logout']);
    Route::get('home','Admin\AppsettingController@index');
    Route::get('/','Admin\AppsettingController@index');
    Route::get('user/show/{id}','Admin\AdminsControllers@show');
    Route::post('user/update/{id}','Admin\AdminsControllers@update');

    Route::get('product','Admin\ProductsControllers@index');
    Route::post('product/deleteImage/{id}','Admin\ProductsControllers@deleteImage')->name('prodcut.deleteImage');
    Route::post('product/updateImage/{id}','Admin\ProductsControllers@updateImage')->name('prodcut.updateImage');
    Route::get('product/create','Admin\ProductsControllers@create')->name('product.add');
    Route::get('product/show/{id}','Admin\ProductsControllers@show');
    Route::post('product/store','Admin\ProductsControllers@store');
    Route::get('product/edit/{id}','Admin\ProductsControllers@edit');
    Route::post('product/update/{id}','Admin\ProductsControllers@update')->name('product.update');
    Route::get('product/destroy/{id}','Admin\ProductsControllers@destroy')->name('product.destroy');
    Route::get('product/color/add/{id}','Admin\ProductsControllers@addColor')->name('product.color.add');;
    Route::get('product/color/destroy/{id}','Admin\ProductsControllers@deleteColor');
    
});
