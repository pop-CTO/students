<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller; 
use Illuminate\Validation\Rule; 
use App\Users;
use Validator;
use Exception;
use App\Http\Resources\UserCollection as UserResource;

class login extends Controller
{
/**  
* login 
* This api will be used to login
* -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
* @param $request Illuminate\Http\Request;
*
* @author ಠ_ಠ Abdelrahman Mohamed 
*/
public function login (Request $request)
{
    try{

        $rules = [
            "email"            => "required|exists:user,email,deleted_at,NULL",
            "password"         => "required",
       ];
       
       
        $messages = [
            "email.required"=>400,
            "email.exists"=>400,
            "password.required"=>400,
     
    ];
       
       
       $validator = Validator::make($request->all(),$rules,$messages);
       if($validator->fails()) {
           return response()->json(['status'=>(int)$validator->errors()->first()]);
       }
       
       //Start logic
       

       
       $user=Users::where('email',$request->email)->first();
       #check if user existis
       if($user == NULL){
        return response()->json(['status' =>203]);
       }
       #check if user Active
       if($user->is_active == 0){
        return response()->json(['status' =>402]);           
       }

       #check if user is_valid
       if($user->is_valid == 0){
        return response()->json(['status' =>408]);           
       }

       #check if user password       
       if (!\Hash::check($request->password, $user->password)) {
        return response()->json(['status' =>203]);
       }

       return new UserResource($user);
       //end logic
       
           }catch(Exception $e) {
               return response()->json(['status' =>404,'error'=>$e->getMessage()]);
             }    
}    
}
