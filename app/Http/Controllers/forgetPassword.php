<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Users;
use App\Http\Controllers\Controller; 
use Illuminate\Validation\Rule; 
use Validator;
use Exception;
class forgetPassword extends Controller
{
/**  
* forgetPassword 
* This api will be used to forgetPassword
* -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
* @param $request Illuminate\Http\Request;
*
* @author ಠ_ಠ Abdelrahman Mohamed 
*/
public function forgetPassword (Request $request)
{
    try{

        $rules = [
       "email"     =>"required|exists:user,email,deleted_at,NULL",

       ];
       
       
        $messages = [
        "email.required"=>400,
        "email.exists"=>409,

       ];
       
       
       $validator = Validator::make($request->all(),$rules,$messages);
       if($validator->fails()) {
           return response()->json(['status'=>(int)$validator->errors()->first()]);
       }
       
       //Start logic
       

       
       $user=Users::where('email',$request->email)->first();
       #check if user Active

        if($user->is_active == 0){
            return response()->json(['status' =>402]);
        }

     $code= $this->verCode();

       $user->code=$code;
       $user->is_valid=0;
       $user->save();
       $this->sendEmail('email.forgetPasword',$user->email,["code"=>$code],'StudentLibrary');
       
       return response()->json(['status' =>200]);
       //end logic
       
           }catch(Exception $e) {
               return response()->json(['status' =>404,'error'=>$e->getMessage()]);
             }    
}

/**  
* validateCode 
* This api will be used to validateCode
* -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
* @param $request Illuminate\Http\Request;
*
* @author ಠ_ಠ Abdelrahman Mohamed 
*/
public function validateCode (Request $request)
{
    try{

        $rules = [
       "email"     =>"required|exists:user,email,deleted_at,NULL",
       "code"     =>"required|exists:user,code",

       ];
       
       
        $messages = [
        "email.required"=>400,
        "email.exists"=>400,
        "code.required"=>400,
        "code.exists"=>408,        

    ];
       
       
       $validator = Validator::make($request->all(),$rules,$messages);
       if($validator->fails()) {
           return response()->json(['status'=>(int)$validator->errors()->first()]);
       }
       
       //Start logic
       

       
       $user=Users::where('email',$request->email)->where('code',$request->code)->first();
       #check if user Active

        if($user == NULL){
            return response()->json(['status' =>408]);
        }

       $user->tempToken=str_random(64);
       $user->save();
       
       return response()->json(['status' =>200,'tmpApiToken'=>$user->tempToken]);
       //end logic
       
           }catch(Exception $e) {
               return response()->json(['status' =>404,'error'=>$e->getMessage()]);
             }    
}

/**  
* resendCode 
* This api will be used to resendCode
* -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
* @param $request Illuminate\Http\Request;
*
* @author ಠ_ಠ Abdelrahman Mohamed 
*/
public function resendCode (Request $request)
{
    try{

        $rules = [
       "email"     =>"required|exists:user,email,deleted_at,NULL",

       ];
       
       
        $messages = [
        "email.required"=>400,
        "email.exists"=>400,      

    ];
       
       
       $validator = Validator::make($request->all(),$rules,$messages);
       if($validator->fails()) {
           return response()->json(['status'=>(int)$validator->errors()->first()]);
       }
       
       //Start logic
       

       
       $user=Users::where('email',$request->email)->first();
       #check if user Active

        if($user == NULL){
            return response()->json(['status' =>408]);
        }

       $code= $this->verCode();
       $user->code=$code;
       $user->save();
       $this->sendEmail('email.forgetPasword',$user->email,["code"=>$code],'StudentLibrary');
       
       return response()->json(['status' =>200]);
       //end logic
       
           }catch(Exception $e) {
               return response()->json(['status' =>404,'error'=>$e->getMessage()]);
             }    
}

/**  
* changePassword 
* This api will be used to changePassword
* -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
* @param $request Illuminate\Http\Request;
*
* @author ಠ_ಠ Abdelrahman Mohamed 
*/
public function changePassword (Request $request)
{
    try{

        $rules = [
            "tmpToken"            => "required|exists:user,tempToken",
            "newPassword"         => "required|min:6",
       ];
       
       
        $messages = [
            "tmpToken.required"=>400,
            "tmpToken.exists"=>400,
            "newPassword.required"=>400,
            "newPassword.min"=>400,         
    ];
       
       
       $validator = Validator::make($request->all(),$rules,$messages);
       if($validator->fails()) {
           return response()->json(['status'=>(int)$validator->errors()->first()]);
       }
       
       //Start logic
       

       
       $user=Users::where('tempToken',$request->tmpToken)->first();
       #check if user Active
       $user->password=\Hash::make($request->newPassword);
       $user->code=NULL;
       $user->tempToken=NULL;
       $user->is_valid=1;
       $user->save();
       
       return response()->json(['status' =>200]);
       //end logic
       
           }catch(Exception $e) {
               return response()->json(['status' =>404,'error'=>$e->getMessage()]);
             }    
}


}
