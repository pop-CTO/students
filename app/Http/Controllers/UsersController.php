<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller; 
use Illuminate\Validation\Rule; 
use App\Users;
use Validator;
use Exception;
use App\Http\Resources\UserCollection as UserResource;
class UsersController extends Controller
{

/**  
* register 
* This api will be used to register new user.
* -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
* @param $request Illuminate\Http\Request;
*
* @author ಠ_ಠ Abdelrahman Mohamed 
*/
public function register (Request $request)
{
    try{

 $rules = [
"name"      =>"required|max:20",
"email"     =>"required|unique:user,email|max:100",
"phone"     =>"required|unique:user,phone|max:20",
"city_id"   =>"required",
"area_id"   =>"required",
"photoUrl"  =>"required",
"password"  =>"required",

];


 $messages = [
 "name.max"=>405,
"phone.max"=>405,
"email.max"=>405,
 "name.required"=>400,
 "email.required"=>400,
 "phone.required"=>400,
 "password.required"=>400,
 "city_id.required"=>400,
 "area_id.required"=>400,
 "photoUrl.required"=>400,
"email.unique"=>550,
"phone.unique"=>405,
];


$validator = Validator::make($request->all(),$rules,$messages);
if($validator->fails()) {
    return response()->json(['status'=>(int)$validator->errors()->first()]);
}

//Start logic

#creat New user

$user=new Users;
$user->apiToken=str_random(64);
$user->name=$request->name;
$user->phone=$request->phone;
$user->email=$request->email;
$user->password=\Hash::make($request->password);
$user->city=$request->city_id;
$user->area=$request->area_id;
$this->SaveFile($user,"photo_url","photoUrl","public/users_images/");
$user->save();
$user=Users::where('id',$user->id)->first();
return new UserResource($user);

//end logic

	}catch(Exception $e) {
        return response()->json(['status' =>404,'error'=>$e->getMessage()]);
      }
     
}


/**  
* visitor 
* This api will be used to register new visitor.
* -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
* @param $request Illuminate\Http\Request;
*
* @author ಠ_ಠ Abdelrahman Mohamed 
*/
public function visitor (Request $request)
{
    try{

 $rules = [
"token"=>"required|unique:user,token",
];


 $messages = [
 "token.required"=>400,
 "token.unique"=>405,
];


$validator = Validator::make($request->all(),$rules,$messages);
if($validator->fails()) {
    return response()->json(['status'=>(int)$validator->errors()->first()]);
}

//Start logic

#creat New user

$user=new Users;
$user->token=$request->token;
$user->isVisitor=1;
$user->apiToken=NULL;
$user->name=NULL;
$user->phone=NULL;
$user->email=NULL;
$user->password=NULL;
$user->city=NULL;
$user->area=NULL;

$user->save();

return response()->json(['status' =>200]);
//end logic

	}catch(Exception $e) {
        return response()->json(['status' =>404,'error'=>$e->getMessage()]);
      }
     
}




/**  
* notificationRegistration 
* This api will be used to notificationRegistration
* -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
* @param $request Illuminate\Http\Request;
*
* @author ಠ_ಠ Abdelrahman Mohamed 
*/
public function notificationRegistration (Request $request)
{
    try{

 $rules = [
"apiToken"     =>"required|exists:user,apiToken,deleted_at,NULL",
"token"        =>"required|unique:user,token",


];


 $messages = [
 "apiToken.required"=>400,
 "apiToken.exists"=>405,
 "token.required"=>400,
 "token.unique"=>405,
];


$validator = Validator::make($request->all(),$rules,$messages);
if($validator->fails()) {
    return response()->json(['status'=>(int)$validator->errors()->first()]);
}

//Start logic

#creat New user

$user=Users::where('apiToken',$request->apiToken)->first();
$user->token=$request->token;
$user->save();
return response()->json(['status' =>200]);
//end logic

	}catch(Exception $e) {
        return response()->json(['status' =>404,'error'=>$e->getMessage()]);
      }
     
}
}
