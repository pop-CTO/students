<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AppSitteng;
use Exception;

class AppSittengs extends Controller
{
    
/**  
* App_sitteng 
* This api will be used to get App_sitteng
* -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
* @param $request Illuminate\Http\Request;
*
* @author ಠ_ಠ Abdelrahman Mohamed 
*/
public function App_sitteng (Request $request)
{
    try{


//Start logic

$AppSitteng=AppSitteng::where('id','1')->first();

if($AppSitteng->police_terms == NULL){
    return response()->json(['status' =>204]);    
}
return response()->json(['status'=>200,'terms'=>$AppSitteng->police_terms]);

//end logic

	}catch(Exception $e) {
        return response()->json(['status' =>404]);
      }
     
}


/**  
* phone && email 
* This api will be used to get phone && email 
* -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
* @param $request Illuminate\Http\Request;
*
* @author ಠ_ಠ Abdelrahman Mohamed 
*/
public function accounts(Request $request)
{
    try{


//Start logic

$AppSitteng=AppSitteng::where('id','1')->select('email','phone')->first();

if($AppSitteng->email == NULL && $AppSitteng->phone == NULL){
    return response()->json(['status' =>204]);    
}
return response()->json(['status'=>200,'accounts'=>$AppSitteng]);

//end logic

	}catch(Exception $e) {
        return response()->json(['status' =>404]);
      }
     
}


/**  
* aboutUs 
* This api will be used to get aboutUs
* -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
* @param $request Illuminate\Http\Request;
*
* @author ಠ_ಠ Abdelrahman Mohamed 
*/
public function aboutUs (Request $request)
{
    try{


//Start logic

$AppSitteng=AppSitteng::where('id','1')->first();

if($AppSitteng->about_us == NULL){
    return response()->json(['status' =>204]);    
}
return response()->json(['status'=>200,'aboutUs'=>$AppSitteng->about_us]);

//end logic

	}catch(Exception $e) {
        return response()->json(['status' =>404]);
      }
     
}
}
