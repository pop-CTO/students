<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\Http\Controllers\Controller; 
use Illuminate\Validation\Rule; 
use Validator;
use Exception;
use App\Users;
use App\Billes;
use App\product;
use App\ordeBilles;
use App\Http\Resources\BillesCollection as BillesCollection;
use App\Http\Resources\OrderCollection as OrderCollection;

class Orders extends Controller
{
/**
 * addOrder
 * This api will be used to addOrder
 * -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
 * @param $request Illuminate\Http\Request;
 *
 * @author ಠ_ಠ Abdelrahman Mohamed
 */
public function addOrder(Request $request)
{
    try {

        $rules = [
            "apiToken"   => "exists:user,apiToken,deleted_at,NULL",
            "token"      => "exists:user,token",
            "productId"  => "required|exists:prodect,id",
            "mount"      => "required"


        ];

        $messages = [
            "mount.required" => 400,
            "productId.exists" => 405,
            "productId.required" => 400,
            "apiToken.exists" => 405, 
            "token.exists" => 405,   

        ];


        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return response()->json(['status' => (int) $validator->errors()->first()]);
        }

//Start logic
#user  and visitor
if($request->apiToken){
$user=Users::where('apiToken',$request->apiToken)->first();
}else{
$user=Users::where('token',$request->token)->first();
}

#check mount product
$product=product::where('id',$request->productId)->first();
if($product->mount < $request->mount ){
    return response()->json(['status'=>205,'mount'=>$product->mount]);
}
#add order
$order=new Order;
$order->user_id=$user->id;
$order->prodect_id=$request->productId;
$order->mount=$request->mount;
$order->save();
#sum
$check=$product->mount -$request->mount;
$product->mount=$check;
$product->save();

return response()->json(['status' => 200]);
//end logic

    } catch (Exception $e) {
        return response()->json(['status' => 404, 'error' => $e->getMessage()]);
    }

}

/**
 * getOrder
 * This api will be used to getOrder
 * -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
 * @param $request Illuminate\Http\Request;
 *
 * @author ಠ_ಠ Abdelrahman Mohamed
 */
public function getOrder(Request $request)
{
    try {

        $rules = [
            "apiToken" => "exists:user,apiToken,deleted_at,NULL",
            "token"      => "exists:user,token",
            "status" => "required|in:wait,request,finish,done",


        ];

        $messages = [
            "status.required" => 400,
            "status.in" => 405,
            "apiToken.exists" => 405, 
            "token.exists" => 405,   

        ];


        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return response()->json(['status' => (int) $validator->errors()->first()]);
        }

//Start logic                           
#user  and visitor
if($request->apiToken){
    $user=Users::where('apiToken',$request->apiToken)->first();
    }else{
    $user=Users::where('token',$request->token)->first();
    }

$order=Order::where('user_id',$user->id)->where('status',$request->status)->with(['order_product'=>function($q){
    $q->with('product_type');
}])->get();

if($order->isEmpty()){
    return response()->json(['status' => 204]);   
}

return response()->json(['status' => 200, 'orders' => new OrderCollection($order)]);
//end logic

    } catch (Exception $e) {
        return response()->json(['status' => 404, 'error' => $e->getMessage()]);
    }

}

/**
 * deleteOrder
 * This api will be used to deleteOrder
 * -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
 * @param $request Illuminate\Http\Request;
 *
 * @author ಠ_ಠ Abdelrahman Mohamed
 */
public function deleteOrder(Request $request)
{
    try {

        $rules = [
            "apiToken" => "exists:user,apiToken,deleted_at,NULL",
            "token"      => "exists:user,token",
            "orderId" => "required|exists:orders,id",


        ];

        $messages = [
            "orderId.required" => 400,
            "orderId.exists" => 405,
            "apiToken.exists" => 405,
            "token.exists" => 405,   

        ];


        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return response()->json(['status' => (int) $validator->errors()->first()]);
        }

//Start logic
#user  and visitor
if($request->apiToken){
    $user=Users::where('apiToken',$request->apiToken)->first();
    }else{
    $user=Users::where('token',$request->token)->first();
    }

$order=Order::where('user_id',$user->id)->where('id',$request->orderId)->delete();

return response()->json(['status' => 200]);

//end logic

    } catch (Exception $e) {
        return response()->json(['status' => 404, 'error' => $e->getMessage()]);
    }

} 


/**
 * addBilles
 * This api will be used to addBilles
 * -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
 * @param $request Illuminate\Http\Request;
 *
 * @author ಠ_ಠ Abdelrahman Mohamed
 */
public function addBilles(Request $request)
{
    try {

        $rules = [
            "apiToken"      => "exists:user,apiToken,deleted_at,NULL",
            "token"          => "exists:user,token",            
            "orders"        =>"required",
            "Total_price"   =>"required",
            "Address"       =>"required",
            "City_id"       =>"required",
            "phone"         =>"required",
            "Area_id"       => "required",
            "Street"        =>"required",
            "Home"          => "required",
            "district"      => "required",  
            "Payment_type"  => "required|in:cache,online"
        ];

        $messages = [
            "orders.required"       => 400,
            "Payment_type.in"       => 405,
            "token.exists"       => 405, 
            "apiToken.exists"       => 405,   
            "Total_price.required"  => 400,
            "Address.required"      => 400,
            "phone.required"        => 400,
            "Street.required"       => 400,
            "Home.required"         => 400,
            "district.required"        => 400,
            "Payment_type.required" => 400,
            "City_id.required"      => 400,
            "Area_id.required"      => 400,     
        ];


        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return response()->json(['status' => (int) $validator->errors()->first()]);
        }

//Start logic
#user  and visitor
if($request->apiToken){
    $user=Users::where('apiToken',$request->apiToken)->first();
    }else{
        if($request->name==NULL){
            return response()->json(['status' =>405 ]);
        }
    
    $user=Users::where('token',$request->token)->first();
    $user->name=$request->name;
    $user->phone=$request->phone;
    $user->city=$request->City_id;
    $user->area=$request->Area_id;
    $user->district = $request->district;
    $user->home = $request->Home;
    $user->street = $request->Street; 
    $user->save();
    }

$billes=new Billes;
$request->phone ==NULL ? $billes->phone=$user->phone:$billes->phone=$request->phone ;
$billes->total_price=$request->Total_price;
$billes->address=$request->Address;
$billes->city=$request->City_id;
$billes->area=$request->Area_id;
$billes->phone=$request->phone;
$billes->street=$request->Street;
$billes->home=$request->Home;
$billes->district=$request->district;
$billes->payment_type=$request->Payment_type;
$billes->user_id=$user->id;
$billes->save();

#get Order Make all order in []
#status request request

foreach($request->orders as $order){

$order_status=Order::where('id',$order)
->update(['status'=>'request']) ;

$order_billes=new ordeBilles;
$order_billes->idOrder=$order;
$order_billes->idBilles=$billes->id;
$order_billes->save(); 

}

return response()->json(['status' => 200]);

//end logic

    } catch (Exception $e) {
        return response()->json(['status' => 404, 'error' => $e->getMessage()]);
    }

}



/**
 * getBilles
 * This api will be used to getBilles
 * -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
 * @param $request Illuminate\Http\Request;
 *
 * @author ಠ_ಠ Abdelrahman Mohamed
 */
public function getBilles(Request $request)
{
    try {

        $rules = [
            "apiToken"      => "exists:user,apiToken,deleted_at,NULL",
            "token"          => "exists:user,token",    
        ];
        $messages = [

            "apiToken.required"     => 400,
            "apiToken.exists"       => 405,  
            "token.exists"          => 405,        
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return response()->json(['status' => (int) $validator->errors()->first()]);
        }

//Start logic
#user  and visitor
if($request->apiToken){
    $user=Users::where('apiToken',$request->apiToken)->first();
    }else{
    $user=Users::where('token',$request->token)->first();
    }

$billes=Billes::where('user_id',$user->id)->with(['orders'=>function($q){
    $q->with('order_product');
}])->get();


return new BillesCollection($billes);

//end logic

    } catch (Exception $e) {
        return response()->json(['status' => 404, 'error' => $e->getMessage()]);
    }

}


/**
 * updateOrder
 * This api will be used to updateOrder
 * -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
 * @param $request Illuminate\Http\Request;
 *
 * @author ಠ_ಠ Abdelrahman Mohamed
 */
public function updateOrder(Request $request)
{
    try {

        $rules = [
            "apiToken"          => "required|exists:user,apiToken,deleted_at,NULL",
            "token"             => "exists:user,token", 


        ];

        $messages = [

            "apiToken.required" => 400,
            "apiToken.exists" => 405,
            "token.exists" => 405,
               

        ];


        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return response()->json(['status' => (int) $validator->errors()->first()]);
        }

//Start logic
#user  and visitor
if($request->apiToken){
    $user=Users::where('apiToken',$request->apiToken)->first();
    }else{
    $user=Users::where('token',$request->token)->first();
    }

#check mount product
$Orders;
foreach($request->orderQuantity as $orderQuantity){

    $order=Order::where('id',$orderQuantity['id'])->first();
    $product=product::where('id',$order->prodect_id)->first();
    $total=$product->mount+$order->mount;

    if($total < $orderQuantity["quantity"] ){
        $Orders[]=$orderQuantity["id"];
    }else{
        $product->mount=$total-$orderQuantity["quantity"] ;
        $product->save();

        $order->mount=$orderQuantity["quantity"] ;
        $order->save();
    }
}
if(!empty($Orders)){
    $order=Order::whereIn('id',$Orders)->with(['order_product'=>function($q){
        $q->with('product_type');
    }])->get();
    return response()->json(['status' => 210, 'orders' => new OrderCollection($order)]); 
}else{
    return response()->json(['status' => 200]);
}

    } catch (Exception $e) {
        return response()->json(['status' => 404, 'error' => $e->getMessage()]);
    }

}

}
