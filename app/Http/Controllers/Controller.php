<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Mail;
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


/**
 * @param string $view path email.blade
 * @param email  $email the user email to send messags
 * @param string $content the content of email
 * @param string $subject  the subject 
 */
public function sendEmail($view,$email,$content,$subject)
{
        $send = Mail::send($view,$content,function ($message) use($subject,$email) {
        $message->from('admin@stdslibrary.com', 'stdslibrary');
        $message->subject($subject);
        $message->to($email);
    });

}

/**
 * @param object $object_table object model 
 * @param string  $cloName name of colum 
 * @param string $fileName the name of file 
 * @param string $path the path to save file  
 */
public function SaveFile($object_table,$cloName,$fileName,$path){
    if(request()->hasFile($fileName))
		{
			$file = request()->file($fileName);
			$filename = str_random(6).'_'.time().'.'.$file->getClientOriginalExtension();
			$file->move($path,$filename);
			$object_table->$cloName = $path.'/'.$filename;
		}
  }
/**
 * @return int random code lenght (6)  
 */
public function verCode(){
    $x=str_random(6);
    return $x;
  }

}
