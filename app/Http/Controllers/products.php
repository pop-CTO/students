<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\product;
use App\Http\Controllers\Controller; 
use Illuminate\Validation\Rule; 
use Validator;
use Exception;
use App\Http\Resources\ProductCollection as ProductCollection;
use App\Http\Resources\ProductInfoCollection as ProductInfoCollection;
use App\Color as color;
use App\Brand as brand;
use App\mainColor;
class products extends Controller
{
/**
 * products
 * This api will be used to get products
 * -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
 * @param $request Illuminate\Http\Request;
 *
 * @author ಠ_ಠ Abdelrahman Mohamed
 */
public function products(Request $request)
{
    try {

        $rules = [
            "typeId" => "required|exists:type,id",


        ];

        $messages = [
            "typeId.required" => 400,
            "typeId.exists" => 405,
        ];


        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return response()->json(['status' => (int) $validator->errors()->first()]);
        }

//Start logic

$product=product::where('type_id',$request->typeId)
->with('product_type')->with('product_color')->with('product_brand')->with('product_images')->get();

#check if product is empty
if($product->isEmpty() ){

    return response()->json(['status'=>204]);
}
        return  new ProductCollection($product);

//end logic

    } catch (Exception $e) {
        return response()->json(['status' => 404, 'error' => $e->getMessage()]);
    }

}

/**
 * productsInfo
 * This api will be used to get productsInfo
 * -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
 * @param $request Illuminate\Http\Request;
 *
 * @author ಠ_ಠ Abdelrahman Mohamed
 */
public function productsInfo(Request $request)
{
    try {

        $rules = [
            "productId" => "required|exists:prodect,id",


        ];

        $messages = [
            "productId.required" => 400,
            "productId.exists" => 405,
        ];


        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return response()->json(['status' => (int) $validator->errors()->first()]);
        }

//Start logic

$product=product::where('id',$request->productId)->with('product_type')
->with('product_color')->with('product_brand')->with('product_images')->get();

#check if product is empty
if($product->isEmpty() ){

    return response()->json(['status'=>204]);
}
$count=$product[0]->n_views;
$product[0]->n_views=$count+1;
$product[0]->save();

return response()->json(['status'=>200,'product'=> new ProductInfoCollection($product[0]) ]);  

//end logic

    } catch (Exception $e) {
        return response()->json(['status' => 404, 'error' => $e->getMessage()]);
    }

}


/**
 * search
 * This api will be used to get search
 * -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
 * @param $request Illuminate\Http\Request;
 *
 * @author ಠ_ಠ Abdelrahman Mohamed
 */
public function search(Request $request)
{
    try {

        $rules = [
            "typeId" => "exists:type,id",
            "color" => "exists:color,id",
            "brand" => "exists:brand,id",


        ];

        $messages = [
            "typeId.exists" => 405,
            "color.exists" => 405,
            "brand.exists" => 405,
        ];


        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return response()->json(['status' => (int) $validator->errors()->first()]);
        }

//Start logic

$product=product::with('product_type')->with('product_color')->with('product_brand')->with('product_images')->where(function($q) use($request){
if($request->typeId){
    $q->where('type_id',$request->typeId);
}

if($request->name){
    $q->where('name','LIKE',$request->name);
}

if($request->brand){
    $q->where('brand','LIKE',$request->brand);
}

if($request->price){
    $q->where('price','<=',$request->price);
}
if($request->color){
    $q->where('color',$request->color);
}

if($request->brand){
    $q->where('brand',$request->brand);
}
})->get();

#check if product is empty
if($product->isEmpty() ){

    return response()->json(['status'=>204]);
}
        return  new ProductCollection($product);

//end logic

    } catch (Exception $e) {
        return response()->json(['status' => 404, 'error' => $e->getMessage()]);
    }

}


/**
 * productClick
 * This api will be used to add N_viwe
 * -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
 * @param $request Illuminate\Http\Request;
 *
 * @author ಠ_ಠ Abdelrahman Mohamed
 */
public function productClick(Request $request)
{
    try {

        $rules = [
            "product_id" => "required|exists:prodect,id",


        ];

        $messages = [
            "product_id.exists" => 405,
            "product_id.required" => 400,            
        ];


        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return response()->json(['status' => (int) $validator->errors()->first()]);
        }

//Start logic

$product=product::where('id',$request->product_id)->first();
$count=$product->n_views;

$product->n_views=$count+1;
$product->save();

return response()->json(['status' => 200]);
//end logic

    } catch (Exception $e) {
        return response()->json(['status' => 404, 'error' => $e->getMessage()]);
    }

}

/**
 * /Product/mostView
 * This api will be used to get mostView
 * -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
 * @param $request Illuminate\Http\Request;
 *
 * @author ಠ_ಠ Abdelrahman Mohamed
 */
public function mostView(Request $request)
{
    try {


//Start logic

$product=product::with('product_type')
->with('product_color')->with('product_brand')->with('product_images')->orderBy('n_views','desc')->take(10)->get();

        return  new ProductCollection($product);

//end logic

    } catch (Exception $e) {
        return response()->json(['status' => 404, 'error' => $e->getMessage()]);
    }

}



/**
 * /color
 * This api will be used to get color
 * -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
 * @param $request Illuminate\Http\Request;
 *
 * @author ಠ_ಠ Abdelrahman Mohamed
 */
public function color(Request $request)
{
    try {


//Start logic

       $color=mainColor::get();

        #check if color is empty
        if($color->isEmpty() ){

            return response()->json(['status'=>204]);
        }

        return  response()->json(['status' => 200, 'color' => $color]);

//end logic

    } catch (Exception $e) {
        return response()->json(['status' => 404, 'error' => $e->getMessage()]);
    }

}



/**
 * /brand
 * This api will be used to get brand
 * -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
 * @param $request Illuminate\Http\Request;
 *
 * @author ಠ_ಠ Abdelrahman Mohamed
 */
public function brand(Request $request)
{
    try {


//Start logic

       $brand=brand::get();

        #check if brand is empty
        if($brand->isEmpty() ){

        return response()->json(['status'=>204]);
         }
    
        return  response()->json(['status' => 200, 'brand' => $brand]);

//end logic

    } catch (Exception $e) {
        return response()->json(['status' => 404, 'error' => $e->getMessage()]);
    }

}

}
