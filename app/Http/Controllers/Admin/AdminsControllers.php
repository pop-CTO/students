<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
class AdminsControllers extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $User = User::find($id);
        return view('admins.edit', compact('User'));

        // return new UserResource(User::find($id));
    }

        /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
                
        $User = User::find($id);
        return view('admins.edit', compact('User'));

        // return new UserResource(User::find($id));
    }


        /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $request->validate([
            "name"      =>"required|max:20",
            "email"     =>"required|max:100",
            "password"     =>"required|max:20",
            ]  );


            $User = User::where('id',$id)->first();
            $User->name=$request->name;
            $User->email=$request->email;
            $User->password=\Hash::make($request->password);
            $User->save();

        return redirect()->back()->with('sendMessageSucc','تم الحفظ بنجاح ');  
    }


}
