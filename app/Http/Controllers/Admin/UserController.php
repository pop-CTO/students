<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Users;
use App\City;
use App\Area;
use Hash;
use Carbon\Carbon;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users=Users::where('isVisitor',0)->get();
        return view('users.index',compact('users'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $User=Users::find($id);
        // if($User==NULL){
        //  $User=Users::onlyTrashed()->where('id',$id)->first();
        // }
        return view('users.show',compact('User'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $User=Users::find($id);
        $City=City::get();
        $Area=Area::get();        

        return view('users.edit',compact('User','City','Area'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
            $request->validate([
                "name"      =>"required|max:20",
                "email"     =>"required|max:100|unique:user,email,".$id,
                "phone"     =>"required|max:20|unique:user,phone,".$id,
                "city_id"   =>"required",
                "area_id"   =>"required",
                "is_active" =>'required'
                ]  );

            $user=Users::find($id);
             
            $user->name     =$request->name;
            $user->phone    =$request->phone;
            $user->email    =$request->email;
            $request->password==NULL?:$user->password=\Hash::make($request->password);
            $user->city =$request->city_id;
            $user->area =$request->area_id;
            $user->is_active=$request->is_active;
            $user->home     =$request->home;
            $user->district =$request->district;
            $user->street   =$request->street;

            if($request->photoUrl){            
                $this->SaveFile($user,"photo_url","photoUrl","public/users_images/");
            }    
            $user->save();
            return back()->with('sendMessageSucc','تم الحفظ بنجاح ');

        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $Users = Users::find($id);
        if($Users->delete()){// this Is Soft Delete
            return redirect()->back()->with('sendMessageSucc','تم الحفظ بنجاح ');
        }
            
    }
}
