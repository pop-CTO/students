<?php

namespace App\Http\Controllers\Admin;

use ConsoleTVs\Charts\Facades\Charts;    
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Users;
use App\Order;
use App\product;
use App\Ads;


class AppsettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

            $RecentUsers = Users::where('isVisitor',0)->orderBy('created_at', 'desc')->take(10)->get();
            $RecentCompanies = Order::where('status','request')->orderBy('created_at', 'desc')->take(10)->get();
    
            $UsersCount = Users::count();
            $Order = Order::count();
            $product = product::count();
            $AdsCount = Ads::count();
    
            $chart_users = Charts::multiDatabase('areaspline', 'highcharts')
            ->title('Users')
            ->colors(['#00acac', '#333333'])
            ->dataset('Users', users::all())
            ->elementLabel("Users")
            ->dimensions(0, 0)
            ->responsive(true)
            ->lastByDay();
    
            $chart_companies = Charts::multiDatabase('areaspline', 'highcharts')
            ->title('Orders ')
            ->colors(['#00acac', '#333333'])
            ->dataset('Orders', Order::all())
            ->elementLabel("Orders")
            ->dimensions(0, 0)
            ->responsive(true)
            ->lastByDay();
    
            return view('home.index', compact('RecentUsers', 
            'UsersCount', 'chart_users','chart_companies',
            'Order','AdsCount','product',
            'RecentCompanies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
