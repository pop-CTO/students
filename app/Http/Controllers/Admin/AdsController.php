<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Ads;
use App\product;

class AdsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $uniReq=Ads::get();
        return view('advertisements.index',compact('uniReq'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $product=product::get();
        return view('advertisements.addAdvertisement',compact('product')); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            "product"   =>"required",
            "img_url"  =>"required",
            "is_active" =>'required'
            
            ];  

        $this->validate($request,$rules);

        $ads=new Ads;
        $ads->prodect_id=$request->product;

         
        $this->SaveFile($ads,"photo_url","img_url","public/users_images/"); 
        $ads->is_active=$request->is_active;
        $ads->save();

        return redirect()->back()->with('sendMessageSucc','تم الحفظ بنجاح ');            
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product=product::get();
        $ads=Ads::find($id);
        return view('advertisements.editAdvertisement',compact('product','ads'));         
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

$ads=Ads::find($id);   
if($request->img_url){
    $this->SaveFile($ads,"photo_url","img_url","public/users_images/");
}
$request->product == NULL ?:$ads->prodect_id=$request->product;
$request->is_active ==NULL ?:$ads->is_active=$request->is_active;         
        $ads->save();

        return redirect()->back()->with('sendMessageSucc','تم الحفظ بنجاح '); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ads=Ads::find($id); 
        $ads->prodect_id == NULL;
        $ads->save();
        $ads->delete();
        return redirect()->back()->with('sendMessageSucc','تم الحفظ بنجاح '); 
    }
}
