<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\product;
use App\Type;
use App\images;
use App\Color;
use App\mainColor;
use App\Brand;

class ProductsControllers extends Controller
{
 /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $uniReq=product::where('deleted_at',NULL)->with('product_images')->get();
        return view('product.index',compact('uniReq'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $product=product::get();
        $Type=Type::get();
        $brand=Brand::get();
        $color=mainColor::get();
        
        return view('product.create',compact('product',"Type","brand",'color')); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            "productName"   =>"required",
            "price"         =>'required',
            "mount"         =>'required',
            "description"   =>'required',
            "discount"      =>'required',
            "Type"          =>'required',
            "brand"         =>'required',
            "color"         =>'required'
            ];  

        $this->validate($request,$rules);

        $product=new product;
        $product->name=$request->productName;
        $product->price=$request->price;
        $product->description=$request->description;
        $product->discount=$request->discount;
        $product->type_id=$request->Type;
        $product->mount=$request->mount;
        $product->brand=$request->brand;
        $product->save();
            if($request->color){
                foreach($request->color as $colors){
                    
                    $addColor=new Color;
                    $addColor->main_color_id=$colors;
                    $addColor->product_id=$product->id;
                    $addColor->save();
                    
                }
            }
        if($request->img_url){
            foreach($request->img_url as $img ){
                $image = $img;
                $name = time().'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('public/users_images/');
                $image->move($destinationPath, $name);
                $imags=new images;
                $imags->url="/public/users_images/".$name;
                $imags->product_id=$product->id;
                $imags->save();
            }
        }

        return redirect()->back()->with('sendMessageSucc','تم الحفظ بنجاح ');            
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product=product::where('id',$id)->with('product_color')->with('product_brand')->first();
    
        $images=images::where('product_id',$id)->get();
        $color=mainColor::get();
        return view('product.show',compact('product','images','color'));     
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product=product::where('id',$id)->first();
        $Type=Type::get();
        $images=images::where('product_id',$id)->get();
        $brand=Brand::get();
        return view('product.edit',compact('product',"Type",'images','brand'));         
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $rules = [
            "productName"   =>"required",
            "img_url"       =>"",
            "price"         =>'required',
            "mount"         =>'required',
            "description"   =>'required',
            "discount"      =>'required',
            "Type"          =>'required',
            "brand"         =>"required"
            ];  

        $this->validate($request,$rules);

        $product=product::where('id',$id)->first();
        $request->productName ==NULL ? :$product->name=$request->productName;
        $request->price ==NULL ? :        $product->price=$request->price;
        $request->description ==NULL ? :        $product->description=$request->description;
        $request->discount ==NULL ? :        $product->discount=$request->discount;
        $request->Type ==NULL ? :        $product->type_id=$request->Type;
        $request->mount ==NULL ? :        $product->mount=$request->mount;
        $request->brand ==NULL ? :        $product->brand=$request->brand;
        $product->save();

        return redirect()->back()->with('sendMessageSucc','تم الحفظ بنجاح '); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = product::find($id);
        if($product->delete()){// this Is Soft Delete
            return redirect()->to("product")->with('sendMessageSucc','تم الحذف بنجاح ');
        }
    }
    
     /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deleteImage($id)
    {
        $images = images::find($id);
        $images->delete();
        return redirect()->back()->with('sendMessageSucc','تم الحفظ بنجاح ');
    }   
    

         /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateImage(Request $request,$id)
    {
        if($request->img_url){
            foreach($request->img_url as $img ){
                $image = $img;
                $name = time().'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('public/users_images/');
                $image->move($destinationPath, $name);
                $imags=new images;
                $imags->url="/public/users_images/".$name;
                $imags->product_id=$id;
                $imags->save();
            }
        }

        return redirect()->back()->with('sendMessageSucc','تم الحفظ بنجاح ');  
    } 


/**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deleteColor($id)
    {
        $color=mainColor::find($id);
        $color->delete();
        return redirect()->back()->with('sendMessageSucc','تم الحفظ بنجاح ');
    }   
    

         /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function addColor(Request $request,$id)
    {
        $rules = [
            "color"   =>"required",

            ];  

        $this->validate($request,$rules);

        if($request->color){
            foreach($request->color as $colors){
                
                $addColor=new Color;
                $addColor->main_color_id=$colors;
                $addColor->product_id=$id;
                $addColor->save();
                
            }
        }

        return redirect()->back()->with('sendMessageSucc','تم الحذف بنجاح ');  
    } 





}
