<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Order;
use App\Billes;
use App\ordeBilles;

class OrderController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $billes =Billes::with('orders')->with('users')->get();
// dd($billes);
        return view('Orders.index', compact('billes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $billes =Billes::where('id',$id)->with('orders')->with('users')->first();
        return view('Orders.show',compact('billes'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function done($id)
    {
        $billes =Billes::where('id',$id)->with(['orders'=>function($q){
            $q->update(['status'=>'done']);

        }])->first();
        return redirect()->back()->with('sendMessageSucc','تم الموافقة  بنجاح ');    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function finish(Request $request, $id)
    {
        $billes =Billes::where('id',$id)->with(['orders'=>function($q){
            $q->update(['status'=>'finish']);

        }])->first();
        return redirect()->back()->with('sendMessageSucc','تم الرفض بنجاح ');    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }   
}
