<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserCollection as UserResource;
use App\Users;
use Exception;
use Illuminate\Http\Request;
use Validator;

class updateProfile extends Controller
{
/**
 * updateProfile
 * This api will be used to updateProfile
 * -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
 * @param $request Illuminate\Http\Request;
 *
 * @author ಠ_ಠ Abdelrahman Mohamed
 */
    public function updateProfile(Request $request)
    {
        try {

            $rules = [
                "apiToken" => "required|exists:user,apiToken,deleted_at,NULL",
                "name" => "max:20",
                "email" => "unique:user,email|max:100",
                "phone" => "unique:user,phone|max:20",
                "photo" => "mimes:jpeg,jpg,png | max:5000",

            ];

            $messages = [
                "apiToken.required" => 400,
                "apiToken.exists" => 405,
                "name.max" => 405,
                "email.unique" => 406,
                "phone.unique" => 407,
                "photo.mimes" => 405,
                "photo.max" => 405,
                "email.max" => 405,
                "phone.max" => 405,
            ];

            $validator = Validator::make($request->all(), $rules, $messages);
            if ($validator->fails()) {
                return response()->json(['status' => (int) $validator->errors()->first()]);
            }

//Start logic

            $user = Users::where('apiToken', $request->apiToken)->first();
            $request->name == null ?: $user->name = $request->name;

            $request->phone == null ?: $user->phone = $request->phone;

            $request->email == null ?: $user->email = $request->email;

            $request->cityId == null ?: $user->city = $request->cityId;

            $request->areaId == null ?: $user->area = $request->areaId;

            $request->district == null ?: $user->district = $request->district;

            $request->home == null ?: $user->home = $request->home;

            $request->street == null ?: $user->street = $request->street;            

            if ($request->photo) {
                $this->SaveFile($user, "photo_url", "photo", "public/users_images/");
            }

            $user->save();

            return new UserResource($user);

//end logic

        } catch (Exception $e) {
            return response()->json(['status' => 404, 'error' => $e->getMessage()]);
        }

    }

/**
 * updatePassword
 * This api will be used to updatePassword
 * -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
 * @param $request Illuminate\Http\Request;
 *
 * @author ಠ_ಠ Abdelrahman Mohamed
 */
    public function updatePassword(Request $request)
    {
        try {

            $rules = [
                "apiToken" => "required|exists:user,apiToken,deleted_at,NULL",
                "oldPassword" => "required|min:6",
                "newPassword" => "required|min:6",

            ];

            $messages = [
                "apiToken.required" => 400,
                "apiToken.exists" => 405,
                "oldPassword.min" => 405,
                "newPassword.min" => 405,
                "oldPassword.required" => 400,
                "newPassword.required" => 400,
            ];

            $validator = Validator::make($request->all(), $rules, $messages);
            if ($validator->fails()) {
                return response()->json(['status' => (int) $validator->errors()->first()]);
            }

//Start logic

            $user = Users::where('apiToken', $request->apiToken)
                ->with('user_city')->with('user_area')->first();
            #check if user password
            if (!\Hash::check($request->oldPassword, $user->password)) {
                return response()->json(['status' => 410]);
            }else{
            $user->password = \Hash::make($request->newPassword);
            $user->save();
            return response()->json(['status' => 200]);
            }
//end logic

        } catch (Exception $e) {
            return response()->json(['status' => 404, 'error' => $e->getMessage()]);
        }

    }
}
