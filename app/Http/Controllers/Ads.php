<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class Ads extends Controller
{
/**  
* ads Object ads
* This api will be used to get all ads
* -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
* @param $request Illuminate\Http\Request;
*
* @author ಠ_ಠ Abdelrahman Mohamed 
*/
public function Ads (Request $request)
{
    try{


//Start logic

$ads=DB::table('Ads')->where('is_active',1)->select('photo_url as photo','prodect_id as idProduct','id','is_active','created_at')->get();


if($ads == NULL || count($ads) == 0 ){
    return response()->json(['status' =>204]);    
}
$data;
foreach($ads as $ad){
    unset($ad->created_at);
    unset($ad->is_active);       
$data[]=$ad;
}
return response()->json(['status'=>200,'ad'=>$data]);

//end logic

	}catch(Exception $e) {
        return response()->json(['status' =>404]);
      }
     
}    
}
