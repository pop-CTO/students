<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class BillesCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return ['status'=>200,'billes'=>$this->resource->map(function ($item) {        
        return [
            "id"            =>$item->id,
            "total_price"   =>$item->total_price,
            "created_at"    =>$item->created_at,
            "address"       =>$item->address,
            "street"        =>$item->street,
            "home"          =>$item->home,
            "payment_type"  =>$item->payment_type,
            "place"         =>$item->place,
            "cityName"      =>$item->city,
            "areaName"      =>$item->area,
            "orders"        =>new OrderCollection($item->orders),

                  ];
    })
];
    }
}
