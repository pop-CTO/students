<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class ProductInfoCollection extends Resource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
    
            return [
                'id' => $this->id,
                'name' => $this->name,
                'description' => $this->description,
                'price' => $this->price,
                'amount' => $this->amount,
                'n_views' =>$this->n_views,
                'brand' =>$this->brand,
                'discount' =>$this->discount,
                'photo' =>$this->product_images,
                'type' =>$this->product_type,
                'color' =>$this->product_color,
                'brand' =>$this->product_brand,
    
            ];
        
    }
}
