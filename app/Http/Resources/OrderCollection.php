<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class OrderCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
     return $this->resource->map(function ($item) {
            return [
                'id' => $item->id,
                'mount' => $item->mount,
                'status' => $item->status,
                'products' => 
                  [
                        'id' => $item->order_product->id,
                        'name' => $item->order_product->name,
                        'description' => $item->order_product->description,
                        'price' => $item->order_product->price,
                        'amount' => $item->order_product->amount,
                        'n_views' =>$item->order_product->n_views,
                        'brand' =>$item->order_product->brand,
                        'discount' =>$item->order_product->discount,
                        'type' =>$item->order_product->product_type,
                        'color' =>$item->order_product->product_color,
                        'brand' =>$item->order_product->product_brand,
                        'photo' =>$item->order_product->product_images,
            
                    ]
                
    
            ];
        });


    }


}
