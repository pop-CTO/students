<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ProductCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return ['status'=>200,'products'=>$this->resource->map(function ($item) {
            return [
                'id' => $item->id,
                'name' => $item->name,
                'description' => $item->description,
                'price' => $item->price,
                'amount' => $item->amount,
                'n_views' =>$item->n_views,
                'brand' =>$item->brand,
                'discount' =>$item->discount,
                'photo' =>$item->product_images,
                'type' =>$item->product_type,
                'color' =>$item->product_color,
                'brand' =>$item->product_brand,
    
            ];
        })
    ];

    }
}
