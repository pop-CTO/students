<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class UserCollection extends Resource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return ["status"=>200,'User'=>[
            'apiToken' => $this->apiToken,
            'name' => $this->name,
            'email' => $this->email,
            'phone' => $this->phone,
            'home' => $this->home,
            'district' => $this->district,
            'street' => $this->street,
            'photoUrl' => $this->photo_url,
            'areaName' =>$this->city,
            'cityName' =>$this->area,

        ]];
    }
}
