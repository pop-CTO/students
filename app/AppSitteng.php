<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppSitteng extends Model
{
    public $timestamps = false;
    public $table="app_sitteng";
}
