<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Billes extends Model
{
    public $table="billes";
    public $timestamps = false;
    public function orders()
    {
        return $this->belongsToMany('App\Order', 'order_billes', 'idBilles', 'idOrder')->withTrashed();
    }



    public function users()
    {
        return $this->belongsTo('App\Users', 'user_id')->withTrashed();
    }
}
