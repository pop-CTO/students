<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public $table="orders";
    public $timestamps = false;

    public function order_product()
    {
        return $this->belongsTo('App\product','prodect_id')->withTrashed();
    }
}
