<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class product extends Model
{
    use SoftDeletes;
    public $table="prodect";
    public $timestamps = false;
    protected $dates = ['deleted_at'];
    protected $hidden = ['pivot'];

    public function product_type()
    {
        return $this->belongsTo('App\Type','type_id');
    }

    public function product_color()
    {
        return $this->belongsToMany('App\mainColor','color')->withTrashed();
    }

    public function product_brand()
    {
        return $this->belongsTo('App\Brand','brand')->withTrashed();
    }

    public function product_images()
    {
        return $this->hasMany('App\images','product_id');
    }
}
