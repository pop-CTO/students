<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class images extends Model
{
    public $table="images";
    public $timestamps = false;
}
