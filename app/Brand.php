<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Brand extends Model
{
    use SoftDeletes;
    public $table="brand";
    public $timestamps = false;
    protected $dates = ['deleted_at'];
    
}
