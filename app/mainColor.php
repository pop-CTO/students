<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class mainColor extends Model
{ 
    use SoftDeletes;
    public $table="main_color";
    public $timestamps = false;
    protected $dates = ['deleted_at'];
    protected $hidden = ['pivot'];
}
