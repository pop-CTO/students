<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ads extends Model
{
    public $table="Ads";
    public $timestamps = false;
    public function product_ads()
    {
        return $this->belongsTo('App\product','prodect_id')->withTrashed();
    }
}
